

I=go install


all: tidy test lint fmt install
tidy:
	go mod tidy

lint:
	golint ./...

test:
	go test ./...

fmt:
	gofmt -w .

install: install-tools

install-tools: install-inlinear install-linear

install-inlinear: install-arstInlex install-arstInparse

install-linear: install-arstLinlex install-arstLinparse

install-arstLinlex:

	$(I) ./cmd/arstLinlex

install-arstLinparse:
	$(I) ./cmd/arstLinparse

install-arstInlex:
	$(I) ./cmd/arstInlex

install-arstInparse:
	$(I) ./cmd/arstInparse


