
# Avocet Tools

The Avocet Compiler provides a set of tools for parsing and
processing reStructuredText and Markdown to form a common JSON
cache, which other tools rely on for data operations.

