package ast

// Block represents a block of parsed content.
type Block struct {
	Type BType  `json:"type" yaml:"type"`
	Name string `json:"type_name" yaml:"type_name"`

	Idrefs []string `json:"identity-references" yaml:"identity-references"`

	File string `json:"file" yaml:"file"`
	Line int    `json:"line" yaml:"line"`

	Content *Content `json:"content" yaml:"content"`
	Key     string   `json:"key,omitempty" yaml:"key,omitempty"`
}

// NewBlock takes a filename and a line number, then returns a
// Block ready for further parsing.
func NewBlock(file string, line int) *Block {
	return &Block{
		File:    file,
		Line:    line,
		Idrefs:  []string{},
		Content: NewContent(),
	}
}

// Set takes a BType and sets the Type and Name attributes.
func (b *Block) Set(t BType) {
	b.Type = t
	b.Name = t.String()
}

// Block adds a new block to the Content.Data field.
func (c *Content) Block(b *Block) {
	c.Data = append(c.Data, b)
}
