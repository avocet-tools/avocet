package ast

import "fmt"

// BType indicates the type of content that rendered the block.
type BType int

const (
	// UNSETBLOCK indicates an unspecified block
	UNSETBLOCK BType = iota

	// PARA indicates a paragraph of text.
	PARA

	// ILIST indicates an itemized list.
	ILIST

	// ILISTITEM indicates an itemized list item.
	ILISTITEM

	// ELIST indicates an enumerated list.
	ELIST

	// ELISTITEM indicates an enumerated list item.
	ELISTITEM

	// DLIST indicates a definition list.
	DLIST

	// DLISTITEM indicates an entry in a definition list.
	DLISTITEM

	// TERM indicates the term in a definition list entry.
	TERM

	// DEF indicates the definition in a definition list entry.
	DEF

	// COMMENT indicates a comment block.
	COMMENT
)

const (
	unsetBType    = "UNSET"
	paraType      = "PARA"
	ilistType     = "ITEMIZE"
	ilistItemType = "ITEMIZE_LIST_ITEM"
	elistType     = "ENUMERATE"
	elistItemType = "ENUMERATED_LIST_ITEM"
	dlistType     = "DEFINITION_LIST"
	dlistItemType = "DEFINITION_LIST_ITEM"
	termType      = "TERM"
	defType       = "DEFINITION"
	commentType   = "COMMENT"
)

var btypeString = map[BType]string{
	UNSETBLOCK: unsetBType,
	PARA:       paraType,
	ILIST:      ilistType,
	ILISTITEM:  ilistItemType,
	ELIST:      elistType,
	ELISTITEM:  elistItemType,
	DLIST:      dlistType,
	DLISTITEM:  dlistItemType,
	TERM:       termType,
	DEF:        defType,
	COMMENT:    commentType,
}

var stringBType = map[string]BType{
	unsetBType:    UNSETBLOCK,
	paraType:      PARA,
	ilistType:     ILIST,
	ilistItemType: ILISTITEM,
	elistType:     ELIST,
	elistItemType: ELISTITEM,
	dlistType:     DLIST,
	dlistItemType: DLISTITEM,
	termType:      TERM,
	defType:       DEF,
	commentType:   COMMENT,
}

// String returns the string representation of the BType.
func (t BType) String() string {
	ret, ok := btypeString[t]
	if ok {
		return ret
	}
	return fmt.Sprintf("UNKNOWN_%d", t)
}

// StringToBType takes a string and returns the BType it
// represents.
func StringToBType(name string) BType {
	ret, ok := stringBType[name]
	if ok {
		return ret
	}
	return UNSETBLOCK
}
