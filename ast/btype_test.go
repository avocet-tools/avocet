package ast

import "testing"

func Test_btype(t *testing.T) {
	data := []struct {
		t BType
		s string
	}{
		{UNSETBLOCK, unsetBType},
		{PARA, paraType},
		{ILIST, ilistType},
		{ILISTITEM, ilistItemType},
		{ELIST, elistType},
		{ELISTITEM, elistItemType},
		{DLIST, dlistType},
		{DLISTITEM, dlistItemType},
		{TERM, termType},
		{DEF, defType},
		{COMMENT, commentType},
	}

	for _, datum := range data {

		if datum.t.String() != datum.s {
			t.Errorf("invalid type string: %s != %s", datum.t, datum.s)
			continue
		}

		st := StringToBType(datum.s)

		if st != datum.t {
			t.Errorf("invalid string type: %s != %s", st, datum.t)
			continue
		}

	}
}
