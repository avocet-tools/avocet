package ast

// Content represents a section of content. The base section of
// a document, the content of a section, the content of a block.
type Content struct {
	Line []*Element `json:"line,omitempty" yaml:"line,omitempty"`

	Meta map[string]any `json:"metadata,omitempty" yaml:"metadata,omitempty"`
	Data []*Block       `json:"data" yaml:"data"`

	Sects []*Section `json:"sections" yaml:"sections"`

	Notes map[string]*Content `json:"notes,omitempty" yaml:"notes,omityempty"`
}

// NewContent initializes and returns a Content block.
func NewContent() *Content {
	return &Content{
		Line: []*Element{},

		Meta: make(map[string]any),
		Data: []*Block{},

		Sects: []*Section{},

		Notes: make(map[string]*Content),
	}
}

// GetString returns the string value for the given key from the
// Content metadata. If the key does not exist or if the key
// does not map to a string value, it returns an empty string.
func (c *Content) GetString(key string) string {
	return getString(key, c.Meta)
}

func getString(key string, m map[string]any) string {
	val, ok := m[key].(string)
	if !ok {
		return ""
	}

	return val
}

// GetContent returns a Content pointer for the given key from
// the Content metadata. If the key does not exist of it the key
// does not map to a Content value, it returns an empty Content
// struct.
func (c *Content) GetContent(key string) *Content {
	return getContent(key, c.Meta)
}

func getContent(key string, m map[string]any) *Content {
	val, ok := m[key].(*Content)
	if !ok {
		return NewContent()
	}
	return val
}

// GetStringArray returns a []string array for the given key. If
// the key does not exist or does not map to a []string, it
// instead returns an empty array.
func (c *Content) GetStringArray(key string) []string {
	return getStringArray(key, c.Meta)
}

func getStringArray(key string, m map[string]any) []string {
	val, ok := m[key].([]string)
	if !ok {
		return []string{}
	}
	return val
}
