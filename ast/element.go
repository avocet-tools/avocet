package ast

import (
	"fmt"

	"github.com/charmbracelet/lipgloss"
)

// Element struct stores content for an inline string. The
// Element.Type specifies how renderer should present the
// content to the builder.
type Element struct {
	Type EType  `json:"type" yaml:"type"`
	Name string `json:"type_name" yaml:"type_name"`

	File string `json:"file" yaml:"file"`
	Line int    `json:"line" yaml:"line"`
	Col  int    `json:"column" yaml:"column"`

	Text string `json:"text" yaml:"text"`

	Key string `json:"key,omitempty" yaml:"key,omitempty"`
}

// NewElement takes a filename, line number and column and returns an
// *Element to the inlinear parser.
func NewElement(file string, line, col int) *Element {
	e := &Element{
		File: file,
		Line: line,
		Col:  col,
	}

	return e
}

// Set takes an element type and updates the Type and Name
// attributes. The Name attribute is the string representation
// of the Type, mainly used in logging functions.
func (e *Element) Set(t EType) {
	e.Type = t
	e.Name = t.String()
}

// Element adds a new Element to the Content.Line field.
func (c *Content) Element(e *Element) {
	c.Line = append(c.Line, e)
}

var (
	emphatic = lipgloss.NewStyle().Italic(true)
	strong   = lipgloss.NewStyle().Bold(true)
	literal  = lipgloss.NewStyle().Bold(true).Italic(true)
)

// String returns the string representation of the Element. It
// is mainly used when printing the Element to stdout.
func (e *Element) String() string {
	switch e.Type {
	case TEXT:
		return e.Text
	case LITERAL:
		return literal.Render(e.Text)
	case STRONG:
		return strong.Render(e.Text)
	case EMPHASIS:
		return emphatic.Render(e.Text)
	}
	lgr.Errorf("Unable to render %s to stdout, returning as unadorned string", e.Type)
	return e.Text
}

// RenderRST renders the Element.Text to reStructuredText
// format.
func (e *Element) RenderRST() string {
	switch e.Type {
	case TEXT:
		return e.Text
	case LITERAL:
		return fmt.Sprintf("``%s``", e.Text)
	case STRONG:
		return fmt.Sprintf("**%s**", e.Text)
	case EMPHASIS:
		return fmt.Sprintf("*%s*", e.Text)
	}
	lgr.Errorf("Unable to render %s to reStructuredText, returning as unadorned string", e.Type)
	return e.Text
}

// RenderMarkdown renders the Element.Text to Markdown format.
func (e *Element) RenderMarkdown() string {
	switch e.Type {
	case TEXT:
		return e.Text
	case LITERAL:
		return fmt.Sprintf("`%s`", e.Text)
	case STRONG:
		return fmt.Sprintf("**%s**", e.Text)
	case EMPHASIS:
		return fmt.Sprintf("*%s*", e.Text)
	}
	lgr.Errorf("Unable to render %s to Markdown , returning as unadorned string", e.Type)
	return e.Text
}

// RenderHTML renders the Element.Text to HTML formaxt.
func (e *Element) RenderHTML() string {
	switch e.Type {
	case TEXT:
		return e.Text
	case LITERAL:
		return fmt.Sprintf("<code>%s</code>", e.Text)
	case STRONG:
		return fmt.Sprintf("<strong>%s</strong>", e.Text)
	case EMPHASIS:
		return fmt.Sprintf("<em>%s</em>", e.Text)
	}
	lgr.Errorf("Unable to render %s to HTML, returning as unadorned string", e.Type)
	return e.Text
}

// RenderLaTeX renders the Element.Text to LaTeX format.
func (e *Element) RenderLaTeX() string {
	switch e.Type {
	case TEXT:
		return e.Text
	case LITERAL:
		return fmt.Sprintf("\\texttt{%s}", e.Text)
	case STRONG:
		return fmt.Sprintf("\\textbf{%s}", e.Text)
	case EMPHASIS:
		return fmt.Sprintf("\\emph{%s}", e.Text)
	}
	lgr.Errorf("Unable to render %s to LaTeX, returning as unadorned string", e.Type)
	return e.Text
}
