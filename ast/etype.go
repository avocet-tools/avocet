package ast

import "fmt"

// EType constants are used to classify Elements.
type EType int

const (

	// UNTEXT indicates an unclassified Element. This should not
	// happen under normal conditions.
	UNTEXT EType = iota

	// TEXT indicates a element with unmodified text.
	TEXT

	// STRONG indicates an element containing bold text.
	STRONG

	// EMPHASIS indicates an element containing emphatic text.
	EMPHASIS

	// LITERAL indicates an element containing literal text.
	LITERAL

	// FOOT indicates the element contains a footnote marker.
	FOOT

	// MARGIN indicates the element contains a margin note
	// marker.
	MARGIN
)

const (
	untextType = "UNSET_TEXT"
	textType   = "INLINE_TEXT"
	strongType = "STRONG"
	emphType   = "EMPHASIS"
	litType    = "LITERAL"
	footType   = "FOOTNOTE"
	marginType = "MARGINALIA"
)

var etypeString = map[EType]string{
	UNTEXT:   untextType,
	TEXT:     textType,
	STRONG:   strongType,
	EMPHASIS: emphType,
	LITERAL:  litType,
	FOOT:     footType,
	MARGIN:   marginType,
}

var stringEtype = map[string]EType{
	untextType: UNTEXT,
	textType:   TEXT,
	strongType: STRONG,
	emphType:   EMPHASIS,
	litType:    LITERAL,
	footType:   FOOT,
	marginType: MARGIN,
}

// String returns the string representation of an element type.
// If it doesn't recognize the type, it returns UNKNOWN_TEXT_%d.
func (t EType) String() string {
	ret, ok := etypeString[t]
	if ok {
		return ret
	}
	return fmt.Sprintf("UNKNOWN_TEXT_%d", t)
}

// StringToElementType takes a name string and returns the
// corresponding EType. If it fails to recognize the type, it
// returns an UNTEXT type.
func StringToElementType(name string) EType {
	ret, ok := stringEtype[name]
	if ok {
		return ret
	}
	return UNTEXT
}
