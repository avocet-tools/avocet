package ast

import "testing"

func Test_etype_string(t *testing.T) {
	data := []struct {
		t EType
		s string
	}{
		{UNTEXT, untextType},
		{TEXT, textType},
		{STRONG, strongType},
		{EMPHASIS, emphType},
		{LITERAL, litType},
		{FOOT, footType},
		{MARGIN, marginType},
	}

	for _, datum := range data {
		if datum.t.String() != datum.s {
			t.Errorf("invalid element type string: %s != %s", datum.t, datum.s)
			continue
		}
		st := StringToElementType(datum.s)

		if st != datum.t {
			t.Errorf("invalid string to element type: %q != %q", st, datum.t)
			continue
		}
	}

}
