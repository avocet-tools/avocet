package ast

import (
	"github.com/charmbracelet/lipgloss"
)

// Section represents a section of content in the file. It
// usually, but doesn't necessarily have a title.
type Section struct {
	Level Level `json:"level" yaml:"level"`

	File string `json:"file" yaml:"file"`
	Line int    `json:"line" yaml:"line"`

	Content *Content `json:"content" yaml:"content"`

	Idrefs []string `json:"identity-reference" yaml:"identity-reference"`
}

// Level indicates the level at which the section occurs.
type Level struct {
	Literal rune `json:"literal" yaml:"literal"`
	Local   int  `json:"local" yaml:"local"`
	Global  int  `json:"global" yaml:"global"`
}

// NewSection takes a filename, line number, rune and local
// level and returns an initialized Section.
func NewSection(file string, line int, r rune) *Section {
	sect := &Section{
		File:    file,
		Line:    line,
		Level:   NewLevel(r),
		Content: NewContent(),
		Idrefs:  []string{},
	}

	return sect
}

// NewLevel takes a rune and a local level and returns an
// initialized Level.
func NewLevel(r rune) Level {
	return Level{
		Literal: r,
	}
}

// Section adds a new section to the Content.Sections field.
func (c *Content) Section(sect *Section) {
	c.Sects = append(c.Sects, sect)
}

var (
	titleFormat = lipgloss.NewStyle().Bold(true)
	charFormat  = lipgloss.NewStyle().Bold(true)
)
