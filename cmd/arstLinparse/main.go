package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet"
)

var cmd = &cobra.Command{
	Use:     "arstLinparse",
	Short:   "Performs linear parse on the given reStructuredText string",
	Version: avocet.Version,
	Run: func(_ *cobra.Command, args []string) {
		avocet.RunLineParser("rst", args)
	},
}

func init() {
	viper.SetDefault("avocet.format", "stdout")

	cmd.PersistentFlags().StringP(
		"format", "F",
		viper.GetString("avocet.format"),
		"Sets the output format")
	viper.BindPFlag("avocet.format", cmd.PersistentFlags().Lookup("format"))
}

func main() {
	cmd.Execute()
}
