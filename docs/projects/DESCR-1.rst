
#######################################
DESCR-1: reStructuredText Linear Lexer
#######################################

.. |DESCR-1| document:: RST Linear Lexer
   :type: descr
   :status: open
   :author: @kennethpjdyer
   :created: 2025-02-11

   Technical description of the reStructuredText linear lexer,
   developed as part of :doc:`SCOPE-1`.

Introduction
************

The linear lexer takes a string as input and returns on demand
tokens for each line found in the strings. The goal of this
project is to implement the basic functionality for the linear
lexer, which we can then expand upon in future projects.

This project description covers the initial phase of work in
:doc:`SCOPE-1`.

Description
***********

The goal of this project is to implement a linear lexer. This
work is split between two packages: 

.. list-table::
   :header-rows: 1

   * - Package
     - Description
   * - :go:package:`~gitlab.com/avocet-tools/avocet/parsers/rst/line/token`
     - Functions, types, and structs used to identify token
       data.
   * - :go:package:`~gitlab.com/avocet-tools/avocet/parsers/rst/line/lexer`
     - Functions, structs, and methods to initialize and analyze
       tokens.

Metrics
*******

- Tokens
  - Implement support for a type to identify linear tokens.
  - Implement a string function to render token types
  human-readable.
  - Implement struct for linear tokens.
  - Implement a string function to return linear tokens.
- Lexer
  - Lexer struct
  - Function to return new Lexer
  - Method to return next token
  - Logging functions to handle lexing errors
- Tool
  - Command-line interface for the linear lexer

