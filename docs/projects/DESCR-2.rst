
#########################################
DESCR-2: reStructuredText Inlinear Lexer
#########################################

.. |DESCR-2| document:: RST Inlinear Lexer
   :type: descr
   :status: open
   :author: @kennethpjdyer
   :created: 2025-02-11

   Technical description of the reStructuredText inlinear lexer,
   developed as part of :doc:`SCOPE-1`.

Introduction
************

The inlinear lexer takes a string as input and returns on demand
tokens for the contents of the strings. The goal of this
project is to implement the basic functionality for the inlinear
lexer, which we can then expand upon in future projects.

This project description covers work in :doc:`SCOPE-1`.

Description
***********

The project splits into two packages:

.. list-table::
   :header-rows: 1

   * - Package
     - Description
   * - :go:package:`~gitlab.com/avocet-tools/avocet/parsers/rst/inline/token`
     - Functions, types, and structs used to identify token
       data.
   * - :go:package:`~gitlab.com/avocet-tools/avocet/parsers/rst/inline/lexer`
     - Functions, structs, and methods to initialize and analyze
       tokens.


Metrics
*******

- Tokens
  - Implement support for a type to identify inlinear tokens.
  - Implement a string function to render token types
  human-readable.
  - Implement struct for inlinear tokens.
  - Implement a string function to return inlinear tokens.
- Lexer
  - Lexer struct
  - Function to return new Lexer
  - Method to return next token
  - Logging functions to handle lexing errors
- Tool
  - Command-line interface for the inlinear lexer



