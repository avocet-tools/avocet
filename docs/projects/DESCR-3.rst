
##########################################
DESCR-3: reStructuredText Inlinear Parser
##########################################

.. |DESCR-3| document:: RST Inlinear Parser
   :type: descr
   :status: open
   :author: @kennethpjdyer
   :created: 2025-02-11

   Technical description of the reStructuredText inlinear parser,
   developed as part of :doc:`SCOPE-1`.

Introduction
************

The inlinear parser takes a string as input and returns on demand
tokens for the contents of the strings. The goal of this
project is to implement the basic functionality for the inlinear
lexer, which we can then expand upon in future projects.

This project description covers work in :doc:`SCOPE-1`.

Description
***********

This project builds out the
:go:package:`~gitlab.com/avocet-tools/avocet/parsers/rst/inline/parser`
package, which covers the parsing of inlinear text.
It also builds out the :go:package:`~gitlab.com/avocet-tools/avocet/ast`
package to cover inline elements.

Metrics
*******

- AST
  - Implement support for a type to identify  AST elements.
  - Implement a string function to render element types
    human-readable.
  - Implement struct for  AST elements.
  - Implement a string function to return AST elements.

- Parser
  - Parser struct
  - Function to return new Parser
  - Method to return next elements
  - Logging functions to handle parser errors

- Tool
  - Command-line interface for the inlinear parser



