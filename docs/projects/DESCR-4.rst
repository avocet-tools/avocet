
#######################################
DESCR-4: reStructuredText Linear Parser
#######################################

.. |DESCR-4| document:: RST Linear Parser
   :type: descr
   :status: open
   :author: @kennethpjdyer
   :created: 2025-02-11

   Technical description of the reStructuredText linear parser,
   developed as part of :doc:`SCOPE-1`.

Introduction
************

The linear parser takes a string as input and returns on demand
tokens for the contents of the strings. The goal of this
project is to implement the basic functionality for the linear
lexer, which we can then expand upon in future projects.

This project description covers work in :doc:`SCOPE-1`.

Description
***********

This project builds out the
:go:package:`~gitlab.com/avocet-tools/avocet/parsers/rst/line/parser`
package, which covers the parsing of inlinear text.
It also builds out the :go:package:`~gitlab.com/avocet-tools/avocet/ast`
package to cover sections and blocks.

Metrics
*******

- AST
  - Implement support for a type to identify  AST blocks.
  - Implement a string function to render block types
    human-readable.
  - Implement struct for AST blocks.
  - Implement a string function to return AST blocks.

- Parser
  - Parser struct
  - Function to return new Parser
  - Method to return next blocks
  - Logging functions to handle parser errors

- Tool
  - Command-line interface for the linear parser



