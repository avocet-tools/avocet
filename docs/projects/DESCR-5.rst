
#######################
DESCR-5: Render Support
#######################

.. |DESCR-5| document:: Render Support
   :status: progress
   :author: kennethpjdyer
   :created: 2025-02-15

   Implement rendering support for string, HTML, and LaTeX.

Introduction
************

In addition to the Go native support for JSON and YAML
rendering, we also want to support string (stdout), HTML, and
LaTeX rendering of the abstract syntax tree. This will allow
developers to evaluate the parser as they implement new language
features.

Description
***********

The render operation will be tied to a series of ``Render%s()``
methods as well as a generic ``Render(string)`` method.

Intermediary parser tools will call these methods directly. The
main renderer will iterate through an ``*ast.Content`` struct to
render the contents.

When we get to rendering block level content, we should consider
how to format to line length and add indentation.


Metrics
*******

How do we measure project's successful completion.

