

##################################
DESIGN-1: reStructuredText Parser
##################################

.. |DESIGN-1| document:: reStructuredText Parser
   :type: design
   :status: open

   This document covers the basic design of the
   reStrucuturedText parser used by Avocet Tools. 

Summary
*******

The reStructuredText parser is intended to produce an abstract
syntax tree, which it provides to downstream projects for
analysis and processing. This document covers the basic design
of how it handles and parses RST.

Behavior
********

In practice, the external behavior of the parser is largely
hidden from the user. The user provides a directory of
reStructuredText files and the tool performs certain operations
based on its contents.

Several tools will be made available to expose the internal
processes of the parser, but it is not expected that users will
employ these tools except for diagnostic purposes.

Description
***********

As with Markdown, reStructuredText is unusual in that the
initial character of a line determine the function of the line.
Rather than jumbling all this together, the Avocet RST Parser
breaks the analyses into two parts: linear and inlinear.

- The linear process focuses on the start of a line and is used
  to identify structural and block-level nodes.

- The inlinear process focuses on the contents of the block and
  is only called in certain cases where this operation is
  necessary.

Linear Lexer and Parser
=======================

The linear lexer and parser are located in the
``parsers/rst/line`` directory. It further divides into three
sub-directories:

Linear Token Package
--------------------

The package is located at ``parsers/rst/line/token``.

The goal of this package is to specify the linear Token struct.
When the lexer receives a string, it breaks it down into lines
which are initialized as ``Token`` pointers.

Linear Token Types
~~~~~~~~~~~~~~~~~~

Linear tokens are typed using a dedicated integer type constant. This
type should feature corresponding string constants that provide
human readable representations of this type.

.. code-block:: go

   package token

   type Type int

   const (
      UNSET Type = iota
   )

   const (
      unsetType = "UNSET_TOKEN"
   )

To facilitate these types we also need type conversion maps and
functions to cast types and strings into their counterparts.
This is mostly done for convenience and logging purposes.

Linear Token Struct
~~~~~~~~~~~~~~~~~~~

The struct for linear tokens is what the lexer initializes and
passes to the parser.

This struct needs a type attribute, a text string, a slice of
runes to facilitate lexical analysis, and a size attribute to
ensure the lexer doesn't pull an out of bounds rune.

Methods on this struct should include the following:

- A ``Set()`` method to reitinilize Text, Runes, and Size.
- A ``Get()`` method to retrieve an arbitrary rune from the
  line and return 0 if the request is out of bounds.
- A ``String()`` method to return a string representation of the
  token.

  .. code-block:: text

     PARA{text="Text..." file="some.txt" line=1}
     
The token should be configured to JSON and YAML output.

Linear Lexer Package
--------------------

The package is located at ``parsers/rst/line/lexer``.

It is takes a string or token slice and returns tokens on demand
as it iterates through the content.

Initialization Functions
~~~~~~~~~~~~~~~~~~~~~~~~

The linear lexer needs two initialization functions. One to
handle string initialization and one to handle token slices.
The token slice is intended for internal use when subparsing
content and will be largely unused during the early stages of
development.

Upon initialization, the lexer should set dedicated read and
Next functions which vary depending on how it was initialized.

Lexer Struct
~~~~~~~~~~~~

The Lexer struct stores its input and position and provides
methods to return the next token in the series.

Lexical Analysis
~~~~~~~~~~~~~~~~

In this stage we want to implement a read function to increment
the lexer, a Next function to return the next token to the
parser, and analysis functions to type the line.

Linear Parser Package
---------------------

The package is located at ``parsers/rst/line/parser``.

Initialization Functions
~~~~~~~~~~~~~~~~~~~~~~~~

The parser should provide two initialization functions based on
whether this is the initial parse or a subparse.

Parser Struct
~~~~~~~~~~~~~

The parser struct initializes a lexer and caches the next two
tokens from the lexer, to allow limited look ahead where
relevant.

Parse Function
~~~~~~~~~~~~~~

The Parse function should return an ast.Content pointer.

Analysis Functions
~~~~~~~~~~~~~~~~~~

The analysis functions should evaluate the tokens returned by
the parser and identify block and structural elements for the
AST.

Inlinear Lexer and Parser
=========================

The inlinear lexer and parser are located in the
``parsers/rst/inline`` directory.

Inlinear Token Package
--------------------

The package is located at ``parsers/rst/inline/token``.

The inlinear token package should echo the linear token but
focus on inlinear types.

Inlinear Lexer Package
----------------------

The package is located at ``parsers/rst/inline/lexer``.

The inlinear lexer package should echo the linear lexer, but
without the next for lexing existing tokens.

Inlinear Parser Package
-----------------------

The package is located at ``parsers/rst/inline/parser``.

The inlinear parser package should echo the design of the linear
parser, but without the need for sub-parsing.

Diagnostics
***********

Each level of the operation: linear and inlinear lexers and
parsers, should initialize a logger to use in reporting the
processes and errors.

Additionally, to aid further development, we should implement
four tools:

* Inlinear Lexer Tool
* Inlinear Parser Tool
* Linear Lexer Tool
* Linear Parser Tool

Each tool should support JSON output by default, but provide
logic to allow alternative renderings for future development.

Validation
**********

Functional Testing
==================

Token Testing
-------------

Each token package should include the following tests:

* Test whether a Type renders to the correct string and whether
  a string renders to the correct type.

* Test token initialization and the retrieval of an arbitrary 
  rune.

Lexer Testing
-------------

Test that a given string is analyzed to the correct results. For
the inlinear lexer, you should also test with spacing and
punctuation.

Performance Testing
===================

Performance will not be evaluated at this stage of development
as it is too early to consider optimization.


Milestones
**********

.. milestone:: v0.2.0

   This milestone covers :doc:`SCOPE-1` work:

   * Sections
   * Paragraph
   * Inline text
   * Strong text
   * Emphatic text
   * Literal text
   * Tools

.. milestone:: v0.2.1

   This milestone covers :doc:`SCOPE-2` work:

   
