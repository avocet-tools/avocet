

###############################
INIT-1: reStructuredText Parser
###############################

.. |INIT-1| document:: reStructuredText Parser
   :author: kennethpjdyer
   :status: open
   :created: 2025-02-11

   Develop a reStructuredText lexer and parser, able to take a
   specification compliant string and return an abstract syntax
   tree for further processing by other tools.

Summary
*******

This document covers the broad initiative to develop a
reStructuredText lexer and parser for general use by Avocet
Tools.

Motivation
**********

Avocet Tools ingest three categories of files from a directory:
source files, resource files, and configuration file.

- Configuration files are read on tool initialization. They
  define how to tool operates, where it looks for other files,
  what it does when it finds them.  

- Resource files are images, videos, stylesheets, and so on.
  These files are not explicitly read by the tool, but they can
  be moved or copied as need.

- Source files provide user content, which the tool must further
  process or utilize.

The goal of this initiative is to develop a lexer and a parser
able to handle source files written in reStructuredText. At this
time it is our intention to use RST as the primary source
format, though support for Markdown and asciidoc may be added at
a future date.

Use Cases
*********

Downstream Tools
================

The primary use case of the reStructuredText parser is a
downstream Avocet tool that requires content or data stored in
an ``.rst`` file.

A separate process will handle reading the file and passing in
the string. The specific target of this initiative is to lex and
parse the string and return an abstract syntax tree to the tool.

Developer Evaluation
====================

This initiative will require several phases to complete. To
guide development, it will introduce several intermediary tools
that allow the user to see how the lexer and parser evaluate
arbitrary strings passed from the command-line.

Requirements
************

- A tool that returns the JSON representation of a token slice.

- A tool that returns the JSON representation of the AST.

- A parser able to handle a specification-compliant
  reStructuredText string

Plan
****

Due to the complexity of this initiative, development will be
broken down into several phases, each building upon the
previous. The phases fall into two broad stages covering basic
and advanced features of the reStructuredText language.

Basic Functionality
===================

The goal of this phase is to implement the basic functionality
of a reStructuredText parser, including the intermediate CLI
tools, AST, and the most basic parsing targets: sections,
paragraphs, strong, emphasis, and literal. The goal of the
second phase is to introduce support for lists: itemized,
enumerated, definition, and field lists as well as footnotes,
endnotes, substitution, and user callouts.

- :doc:`SCOPE-1`
- :doc:`SCOPE-2`


Advanced Functionality
==========================

The goal of this phase is to implement directives, which require
additional logic to decide how to handle directive content. The
parser will then be expanded to process interpreted text. This
moves from the basic title-key pairings up to verified and
unverified links, and roles.

