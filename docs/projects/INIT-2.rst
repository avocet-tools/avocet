

########################
INIT-2: Avocet Compiler
########################

.. |INIT-2| document:: Avocet Compiler
   :author: kennethpjdyer
   :created: 2025-02-12
   :dependencies: INIT-1

   Initiative to develop a compiler based on the initial work of
   the reStructuredText lexer and parser. The user should be
   able to run a single command to compile validated source
   content into a cache, then update the cache
   incrementally with user updates.


Summary
*******

Developer a content compiler able to read multiple file formats,
parse relevant source files, store the validated results to
a cache, then update the cache as the user updates content.

Motivation
**********

Downstream tools need data on complete projects and sometimes
also on external projects as well as an input. Following work on
:doc:`INIT-1`, the parser should be in a good state to process a
string of reStructuredText and return an AST Content pointer.
The compiler adds file system logic to find source files, parse
them, and initialize a Project, which contains all parsed
Documents, validated and ready for processing by Avocet Tools.

Use Cases
*********

Downstream Tools
================

Downstream tools should start by opening the cache and calling
an update function to check source files for any changes. In
order to get this cache, we need a base process to compile
projects and cache them so that they're ready for use.

Diagnostics
===========

When the parser iterates over files, it sometimes encounters
errors such as headlines that are too short, inline elements
without closing tags, directives that contain the wrong field
options, and invalid domains

The parser should emit errors as it runs, but we also want to
pool these errors and report all of them whenever the
compilation or update process runs.


Requirements
************

* Compile project source files to a cache.

* Update process to perform incremental changes to the cache
  instead of reading the entire source repository from scratch.

* Pass errors from parses up to the Project. Report errors,
  update the error cache on subsequent compilations.




