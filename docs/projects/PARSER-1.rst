
###############################
PARSER-1: Inlinear Token Types
###############################

.. |PARSER-1| issue:: Inlinear Token Types
   :type: parser
   :parent: PROJECT-1
   :status: closed
   :created: 2025-02-11

   Implement type definitions for inlinear lexical analysis for
   :issue:`PROJECT-1`. This should include:

Description
***********

The goal of this issue is to implement basic support for type
functions

* The type definition
* Iota types for ``UNSET``, ``EOL``, ``SPACE``, and ``TEXT``.
* String constants for each type
* A function to render types to strings
* A function to render strings to types
* A test to ensure accurate type conversion

Review
******

Work was straightforward and quick. No anomalies encountered in
the process. Upon completion, the inlinear ``token`` package is
ready for typing Tokens.

Logs
****

.. logs::

   .. log:: Created
      :date: 2025-02-11





