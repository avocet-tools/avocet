
##############################
PARSER-10: Inlinear Lexer Tool
##############################

.. |PARSER-10| issue:: Inlinear Lexer Tool
   :parent: PROJECT-4
   :dependencies: PARSER-3
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: progress

   Build a tool that performs a inlinear lex and then prints the
   tokens to stdout.


Description
***********

The goal of this project is to take an arbitrary string from the
command-line and print the results of inlinear lexical analysis
as a slice of tokens to stdout.  The token can be formatted to a
string, JSON, or YAML by a ``--format`` option.

