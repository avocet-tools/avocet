
##############################
PARSER-11: Inlinear Parser Tool
##############################

.. |PARSER-10| issue:: Inlinear Parser Tool
   :parent: PROJECT-4
   :dependencies: PROJECT-2
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Build a tool that performs a inlinear parse and then prints the
   tokens to stdout.


Description
***********

The goal of this project is to take an arbitrary string from the
command-line and print the results of inlinear semantic analysis
as a slice of tokens to stdout.  The token can be formatted to a
string, JSON, or YAML by a ``--format`` option.

