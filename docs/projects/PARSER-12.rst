
############################
PARSER-12: Linear Token Type
############################

.. |PARSER-12| issue:: Linear Token Types
   :parent: PROJECT-5
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement support for linear token types.

Description
***********

The goal of this ticket is to implement the basic constants for
the linear token types. Basic types are ``UNSET``, ``EOF``,
``TEXT``, and ``EMPTY``.

The work should include functions to render type to string and
string to type as well as tests to evaluate the success of these
operations.




