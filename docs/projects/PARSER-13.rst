
##############################
PARSER-13: Linear Token Struct
##############################

.. |PARSER-13| issue:: Linear Token Structs
   :parent: PROJECT-5
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement support for linear token struct.

Description
***********

The goal of this ticket is to implement to linear token struct
as well as functions required for its initialization. Prepare
the token for rendering to JSON, YAML, and String.



