
##############################
PARSER-14: Linear Lexer Struct
##############################

.. |PARSER-14| issue:: Linear Lexer Struct
   :parent: PROJECT-5
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement struct for linear lexer.

Description
***********

The goal of this ticket is to implement the basic struct for the
linear lexer, including initialization functions and a basic
next function to retrieve the next token from the sequence.



