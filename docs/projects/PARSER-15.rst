
##############################
PARSER-15: AST Block Types 
##############################

.. |PARSER-15| issue:: AST Block Types
   :parent: PROJECT-6
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done
   :dependencies: PROJECT-5

   Implement support for block typing in the abstract syntax
   tree.

Description
***********

Implement a type definition for block types in the abstract
syntax tree. Include constants for the ``PARA`` as well as
support for rendering types to and from strings.



