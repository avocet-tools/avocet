
##############################
PARSER-16: AST Block Struct
##############################

.. |PARSER-16| issue:: AST Block Struct
   :parent: PROJECT-6
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done
   :dependencies: PARSER-15

   Implement struct in abstract syntax tree for blocks.

Description
***********

Implement a block struct in the abstract syntax tree. Include
initialization functions for the block and methods to set the
block type.



