
##############################
PARSER-17: AST Section Struct
##############################

.. |PARSER-17| issue:: AST Section Struct
   :parent: PROJECT-6
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done
   :dependencies: PARSER-15

   Implement struct in abstract syntax tree for sections. 

Description
***********

Implement a section struct in the abstract syntax tree. Include
initialization functions for the section.



