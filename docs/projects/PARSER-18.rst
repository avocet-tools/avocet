
##############################
PARSER-18: AST Content Struct
##############################

.. |PARSER-18| issue:: AST Content Struct
   :parent: PROJECT-6
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done
   :dependencies: PARSER-16 PARSER-17

   Implement struct in abstract syntax tree for content. 

Description
***********

Implement a content struct in the abstract syntax tree. Include
initialization functions for the content.



