
###############################
PARSER-19: Linear Parser Struct
###############################

.. |PARSER-19| issue:: Parser Struct
   :parent: PROJECT-6
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status:done
   :dependencies: PARSER-16 PARSER-17 PARSER-18

   Implement linear reStructuredText parser.

Description
***********

Implement a parser for linear reStructuredText. The parser
should begin as a struct that initializes the linear lexer.
Implement a Parse function that returns an AST Content pointer.

The analysis process should be able to identify a paragraph.

Tests should evaluate whether the parser returns an empty
Content pointer. It should also take a Document as an argument
for storing file-level information during the parse.


