
###############################
PARSER-2: Inlinear Token Struct
###############################

.. |PARSER-2| issue:: Inlinear Token Struct
   :reporter: kennethpjdyer
   :assignee:: kennethpjdyer
   :status: done
   :parent: PROJECT-1
   :dependencies: PARSER-1

   Implement the Token struct with relevant functions to
   initialize and retrieve data.


Description
***********

Implement the ``Token`` struct for the inlinear lexer. This
should include a function to initialize the token and a private
method that performs the initial analysis, setting values on the
token based on the single rune content.

Tests should evaluate whether the token analysis returns the
correct results.

Review
******

Change made without difficulty. No anomalies encountered during
development.


Logs
****

.. logs:: PARSER-2


