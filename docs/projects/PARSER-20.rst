
#######################################
PARSER-20: Render HTML for Inlinear AST
#######################################

.. |PARSER-20| issue:: Render HTML for Inlinear AST
   :parent: PROJECT-7
   :assignee: kennethpjdyer
   :reporter: kennethpjdyer
   :status: done

   Implement framework for HTML, stdout, and LaTeX rendering on
   inlinear elements.


Description
***********

Update the AST to support HTML, stddout, and LaTeX rendering on
inlinear elements. Future work will include updating the
rendering functions as new features get added to the language.


