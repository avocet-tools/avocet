
#############################################
PARSER-21: reStructuredText Linear Lexer Tool
#############################################

.. |PARSER-21| issue:: reStructuredText Linear Lexer Tool
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement a linear lexer tool for reStructuredText. 
   Part of PROJECT-8.

Description
***********

The linear lexer tool for reStructuredText is used to evaluate
tokens passed from the lexer to the parser. Results should print
to stdout in JSON and YAML formats.

