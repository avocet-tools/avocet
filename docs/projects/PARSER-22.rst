
##############################
PARSER-22: Linear Parser Tool
##############################

.. |PARSER-22| issue:: Linear Parser Tool
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement a linear parser tool. The purpose of this tool is
   to observe what the parser returns to the compiler.

Description
***********

Developers at work implementing the reStructuredText
specification and domain specifications for the parser need to
see the unmodified data that gets passed to the compiler.

This tool will take an arbitrary string and return the
ast.Content block for the given text. It then renders the block
to JSON, YAML, LaTeX, HTML, reStructuredText, Markdown, or
stdout.

