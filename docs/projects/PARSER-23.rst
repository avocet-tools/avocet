
##########################
PARSER-23: Target Support
##########################

.. |PARSER-23| issue:: Target Support
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :scope: SCOPE-2
   :status: done


Description
***********

Update the linear parser to support targets and identity
references. The inline counterparts of both will be addressed in
a future release.

Targets
=======

Targets are used to prepare link targets for interpreted text.
The goal is to set the key or the title and key at the bottom of
a document, allowing for simplified references in text.

Target Example:

.. code-block:: rst

   .. _key: www.example.com

   .. _`tick key`: www.example.com

Target resolution is not within the scope of this release. Our
goal is only to catch the map the references for later callouts.

Identity Reference
==================

Identity reference is used to update sections and blocks for
cross-reference links.


Identity reference example:

.. code-block:: rst

   .. _section-key:

   Section Title
   =============

Cross-reference links are handled by roles, the resolution of
which will be handled with interpreted text as part of a future
release.
