
#####################################
PARSER-24: Render Support for Blocks
#####################################

.. |PARSER-24| issue:: Render Support for Blocks
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :scope: SCOPE-2
   :status: done

   Extend linear parser render support to cover HTML, LaTeX,
   reStructuredText, and Markdown.


Description
***********

The initial implementation of render support for Content, Block,
and Section only covers JSON, YAML, and stdout. We want to
expand this support for feature parity with the inlinear
renderer.

Don't abstract the render functions. We need a dedicated system
to allow building in parallel.

