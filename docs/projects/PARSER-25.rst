
##########################
PARSER-25: Itemized Lists
##########################

.. |PARSER-25| issue:: Itemized Lists
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement support for itemized lists.

Description
***********

Update the linear parser and renderers to support itemized
lists.  The Avocet parser will only support the following
itemized list characters: ``-``, ``+``, and ``*``.
