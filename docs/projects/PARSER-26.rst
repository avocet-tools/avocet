
############################
PARSER-26: Enumerated Lists
############################

.. |PARSER-26| issue:: Enumerated Lists
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement support for enumerated lists.

Description
***********

Update the linear parser and renderers to support itemized
lists.  The Avocet parser should support hash (``#.``),
alphabetic (``a.`` or ``A.``), and numeric (``1.``) formats.
