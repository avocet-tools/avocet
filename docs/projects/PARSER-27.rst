
##########################
PARSER-27: Checklists
##########################

.. |PARSER-27| issue:: Checklists
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement support for checklists.

Description
***********

Update the linear parser and renderers to extend itemized lists
to support checklist. A checklist item is a distinct itemized
list item that indicates the status of the note.  For example:

.. code-block:: rst

   * [ ] Open Item
   * [-] Progress Item
   * [x] Done Item

This will require additional configuration options to specify
arbitrary key, name, and quality values.

Review
******

This work doesn't feature any rendering support. This will be
added later as part of a Bullet Journal history tool.

