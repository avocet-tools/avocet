
###########################
PARSER-28: Definition Lists
###########################

.. |PARSER-28| issue:: Definition Lists
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement support for definition lists.

Description
***********

Update the linear parser and renderers to support definition
lists.

.. code-block:: rst

   term
      definition

This will require additional configuration options to specify
arbitrary key, name, and quality values.

Note, extended fields will be introduced in a
:issue:`PARSER-30`. 
