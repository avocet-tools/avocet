
###########################
PARSER-29: Field Lists
###########################

.. |PARSER-29| issue:: Field Lists
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: progress

   Implement support for field lists.

Description
***********

Update the linear parser and renderers to support field 
lists.

.. code-block:: rst

   :key: value

Configuration changes will include specification for document
and section level configuration specifications.  That is, the
configuration will tell the parser whether it should treat the
field value as a string, integer, boolean, or Content block.

