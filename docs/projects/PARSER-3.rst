
#############################
PARSER-3: Inlinear RST Lexer
#############################

.. |PARSER-3| issue:: Inlinear RST Lexer
   :parent: PROJECT-1
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done
   :dependencies: PARSER-1 PARSER-2

   Implement the struct for the inlinear reStructuredText lexer.


Description
***********

Implement the struct and initialization functions for the
inlinear Lexer.  Include the read and next logic to analyze the
token and pass it on to the parser.

Tests should evaluate lexer size and the initial token results.

Review
******

Change implemented without trouble. No anomalies encountered
during development.

Logs
****

.. logs:: PARSER-3

