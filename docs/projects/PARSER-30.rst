
##################################
PARSER-30: Definition Classifiers
##################################

.. |PARSER-30| issue:: Definition Classifier
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement support for classifiers for definitions.

Description
***********

Updates the definition list logic to support classifiers.


.. code-block:: rst

   term : class-1 : class-2
      definition

