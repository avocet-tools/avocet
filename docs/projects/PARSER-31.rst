
####################
PARSER-31: Footnotes
####################

.. |PARSER-31| issue:: Footnotes
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement support for footnotes. 

Description
***********

Updates the parser to support footnotes.


.. code-block:: rst

   .. [note] Description

      body elements

This corresponds to an inline footnote reference:

.. code-block:: rst

   Text with footnote.[note]_

Unlike the RST specification, Avocet will not support automatic
footnotes. Instead, users must specify a key to match to the
footnote.

