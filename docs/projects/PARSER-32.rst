
########################
PARSER-32: Margin-notes
########################

.. |PARSER-32| issue:: Margin-notes
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement support for margin notes. 

Description
***********

Updates the parser to support margin-notes.

Note that margin-notes are not part of the reStructuredText
specification. The goal is to allow a writer to add notes that
appear in the margins of a document, similar to how word
processors handle comments.

Eventually, we'll implement tools to manage and track
margin-notes, translate them to work tickets, and like features.

.. code-block:: rst

   .. [[note]] Description

      body elements

This corresponds to an inline margin-note reference:

.. code-block:: rst

   Text with margin-note.[note]__


