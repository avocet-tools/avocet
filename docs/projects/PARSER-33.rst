
###########################
PARSER-33: Comment Support
###########################

.. |PARSER-33| issue:: Comment Support 
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   Implement support for comments.

Description
***********

Updates the parser to support for comments.

For example:

.. code-block:: rst

   .. comment text.

   
Avocet support should extend to include specialized comments:

.. code-block:: rst

   .. TODO: Title

   .. FIXME: Title

   .. BUG: Title

The parser configuration should provide support for arbitrary
comment keys. It should also support numeric identifiers to
connect a comment key with a ticket document.
