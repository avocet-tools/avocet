
########################
PARSER-34: Upstream Data
########################

.. |PARSER-34| issue:: Upstream Data
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   The goal of this project is to upstream data from blocks to
   the document-level, where they can feature in rendering
   operations.


Description
***********

Certain reStructuredText objects like targets and footnotes,
should render from the document-level.  For instance, when
replacement text is set at the bottom of a file but the replaced
inline text occurs somewhere above.

The goal of this project is to reconfigure the linear parser to
store data during operations and pass it upstream so that the
eventual ast.Document can collect and set it for later
processing.

