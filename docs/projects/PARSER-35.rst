
#################################
PARSER-35: Base Directive Support
#################################

.. |PARSER-35| issue:: Base Directive
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   Implement base directive support.


Description
***********

Implement the base support for directives.  The goal of this
ticket is to code the parser to recognize a default directive
and a domain-specified directive and to set the key and the base
argument on the directive.

At this stage, the parser should ignore directive content.

It should also implement support for the ``default-domain``
directive and to pass changes to this setting down into
sub-parsers and the inlinear parser.


