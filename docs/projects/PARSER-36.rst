
################################
PARSER-36: Preserved Directives
################################

.. |PARSER-36| issue:: Preserved Directives
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   Implement support for preserved directives.


Description
***********

Preserved directives are those that parse the initial field data
of directive content and log the rest as a preserved value on
the Content.Meta map.

