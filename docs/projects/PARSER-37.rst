
###############################
PARSER-37: Sub-parsed Directive
###############################

.. |PARSER-37| issue:: Subparsed Directive
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   Implement support for subparsed directives.


Description
***********

Implements support for sub-parsed directives. These directives
parse directive content as a dedicated section.  Include support
for inclusion/exclusion of the initial argument in the parse.

