
################
PARSER-38: Roles
################

.. |PARSER-38| issue:: Roles
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   Implement support for default and domain roles.


Description
***********

Implement support for interpretive text elements where the user
specifies the role and domain.

.. code-block:: rst

   - :role:`key`
   - :role:`Title <key>`
   - :dom:role:`key`
   - :dom:role:`Title <key>`


Note, Avocet will likely not support the reverse form of roles
at this time:

.. code-block:: rst

   - `key`:role`
