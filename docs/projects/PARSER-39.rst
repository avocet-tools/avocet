
########################
PARSER-39: Default Roles
########################

.. |PARSER-39| issue:: Default Role
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   Implement support for default roles.

Description
***********

Default roles are set using the ``default-role`` directive. Once
set, any unspecific instances of interpreted text are
interpreted as though of this default role.

For example:

.. code-block:: rst

   .. default-role:: ref
      :domain: rst

   `title <key>`

The instance of interpreted text on the final line renders as
the ``:rst:ref:`` role.

