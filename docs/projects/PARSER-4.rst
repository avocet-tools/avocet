
##########################
PARSER-4: AST Element Type
##########################

.. |PARSER-4| issue:: AST Element Type
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :parent: PROJECT-2
   :dependencies: PARSER-3
   :status: closed

   Implement type definition to classify Element structs
   in the abstract syntax tree.


Description
***********

Implement type definition for Elements. Begin with ``TEXT``.
Include support to render the type to and from strings.

Test rendering types to and from strings.


Logs
====

.. logs:: PARSER-4

