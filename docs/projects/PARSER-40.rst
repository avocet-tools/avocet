
###############################
PARSER-40: Anonymous Hyperlinks
###############################

.. |PARSER-40| issue:: Anonymous Hyperlinks
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   Implement support for anonymous hyperlinks.


Description
***********

Implements support for anonymous hyperlinks. These are
hyperlinks with hyperlink references and possibly title text set
for which the parser performs no further calculations.

.. code-block:: rst

   `Title <http://example.com>`__

