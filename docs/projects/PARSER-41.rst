
###########################
PARSER-41: Named Hyperlinks
###########################

.. |PARSER-41| issue:: Named Hyperlinks
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   Implement support for named hyperlink references.


Description
***********

Implement support for named hyperlink references. A named
hyperlink reference sets a name and link value on the return
targets and throws an error if it encounters a duplicate.

.. code-block:: rst

   `Key <address>`_

Named reference set the address and the key text when it's missing.
