
#######################
PARSER-42: Substitution
#######################

.. |PARSER-42| issue:: Substitution 
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: open

   Implement support for substitution.


Description
***********

Implement support for the ``replace`` directive and the
substitution inline element.

.. code-block:: rst

   .. |key| replace:: text

   Paragraph with |key| replaced.
