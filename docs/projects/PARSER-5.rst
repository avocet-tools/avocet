
#############################
PARSER-5: AST Element Struct
#############################

.. |PARSER-5| issue:: AST Element
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :parent: PROJECT-2
   :status: done

   Implement the struct in the abstract syntax tree for
   Elements. 

Description
***********

The goal of this issue is to implement an inline element struct
in the AST for use by the inlinear parser.

This should include a type (as specified in :issue:`PARSER-4`)
and take a text string, a filename, line and column numbers for
logging.

Tests should cover initialization, String, JSON, and YAML
rendering.

Logs
====

.. logs:: PARSER-5

