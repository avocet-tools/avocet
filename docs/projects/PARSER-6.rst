
##############################
PARSER-6: Inlinear RST Parser
##############################

.. |PARSER-6| issue:: Inlinear RST Parser
   :parent: PROJECT-2
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :dependencies: PARSER-5
   :status: done

   Implement the reStructuredText inlinear parser.


Description
***********

Implement the basics of the inlinear reStructuredText parser.
This should include the Parser struct as well as initialization
functions to take a string and set up the parser to run.

The parse function should return a slice of Element pointers.

Tests should evaluate whether the parser returns an empty array
or if it returns a single inline text element.

Logs
====

.. logs:: PARSER-6

