
############################
PARSER-7: Parse Strong Text
############################

.. |PARSER-7| issue:: Strong Text
   :parent: PROJECT-3
   :dependencies: PROJECT-2
   :reporter: kennethpjdyer
   :assignee: kennethpjder
   :status: done

   Implement support for strong text in the inlinear
   reStructuredText lexer and parser.

Description
***********

Extend support in the inlinear reStructuredText lexer and parser
to cover strong text.

Update existing tests to accommodate these new tokens and
elements.

