
#############################
PARSER-8: Parse Emphatic Text
#############################

.. |PARSER-8| issue:: Emphatic Text
   :parent: PROJECT-3
   :dependencies: PROJECT-2
   :reporter: kennethpjdyer
   :assignee: kennethpjder
   :status: done

   Implement support for emphatic text in the inlinear
   reStructuredText lexer and parser.

Description
***********

Extend support in the inlinear reStructuredText lexer and parser
to cover emphatic text.

Update existing tests to accommodate these new tokens and
elements.

