
#############################
PARSER-9: Parse Literal Text
#############################

.. |PARSER-9| issue:: Literal Text
   :parent: PROJECT-3
   :dependencies: PROJECT-2
   :reporter: kennethpjdyer
   :assignee: kennethpjder
   :status: done

   Implement support for literal text in the inlinear
   reStructuredText lexer and parser.

Description
***********

Extend support in the inlinear reStructuredText lexer and parser
to cover literal text.

Update existing tests to accommodate these new tokens and
elements.

