
#################################################
PHASE-1: Basic Inlineagr reStructuredText Parser
#################################################

.. |PHASE-1| issue:: Basic Inlinear RST Parser
   :type: phase 
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: progress

   Implement a basic inlinear reStructuredText lexer and parser.

Description
***********

The parser is a fundamental component to most Avocet Tools. The
goal of this phase is to implement the most basic functionality
for the parser.

In this phase we want to implement support for the following
language features:

* Element Support:
  * Inline text 
  * Strong text
  * Emphatic text
  * Literal text

To facilitate development, this phase also aims to introduce
four command-line tools. These tools will be used to evaluate
intermediate stages of lexing and parsing and guide future
development.

.. list-table::
   :header-rows: 1
   :widths: 30 70

   * - Tool
     - Description
   * - ``arstInLex``
     - Evaluates the inlinear lexer, returning the JSON
       representation of tokens for the contents of a line.
   * - ``arstInParse``
     - Evaluates the inlinear parser, returning the JSON
       representation of a slice of ast.Elements parsed from the
       given line of text.

Projects
========

.. tickets::

   PROJECT-1
   PROJECT-2
   PROJECT-3
   PROJECT-4





