#############################################
PHASE-2: Basic Linear reStructuredText Parser
#############################################

.. |PHASE-2| issue:: Basic Linear RST Parser
   :type: phase 
   :reporter: @kennethpjdyer
   :assignee: @kennethpjdyer
   :status: backlog

   Implement a basic linear reStructuredText lexer and parser.

Description
***********

The parser is a fundamental component to most Avocet Tools. The
goal of this phase is to implement the most basic functionality
for the parser.

In this phase we want to implement support for the following
language features:

* Structural Support:
  * Sections
  * Section titles
  * Targets
* Block Support:
  * Paragraphs

To facilitate development, this phase also aims to introduce
four command-line tools. These tools will be used to evaluate
intermediate stages of lexing and parsing and guide future
development.

.. list-table::
   :header-rows: 1
   :widths: 30 70

   * - Tool
     - Description

   * - ``arstLex``
     - Evaluate linear lexer, returning the JSON representation
       of a token for each line of the given string.
   * - ``arstParse``
     - Evaluates linear parser, returning the JSON
       representation of the AST for the given string.

Projects
********

.. tickets::

   PROJECT-5
   PROJECT-6



