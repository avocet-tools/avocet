
##############################
PROJECT-1: Inlinear RST Lexer
##############################

.. |PROJECT-1| issue:: Inlinear RST Lexer
   :type: project
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement the inlinear reStructuredText lexer as part of
   :issue:`PHASE-1`.

Description
***********

The goal of this project is to implement the inlinear
reStructuredText lexer. At the end of this project we should
we should have the basic implementation of the lexer and
relevant tests.

Tickets
=======

* :issue:`PARSER-1`
* :issue:`PARSER-2`
* :issue:`PARSER-3`


