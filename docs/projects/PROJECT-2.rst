
##############################
PROJECT-2: Inlinear RST Parser
##############################

.. |PROJECT-2| issue:: Inlinear RST Parser
   :type: project
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement the inlinear reStructuredText lexer as part of
   :issue:`PHASE-1`.

Description
***********

The goal of this project is to implement the inlinear
reStructuredText parser. At the end of this project we should
have a parser able to handle basic processing.

Tickets
=======

.. tickets::

   PARSER-4
   PARSER-5
   PARSER-6



