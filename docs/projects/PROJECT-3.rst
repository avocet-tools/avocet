
######################################
PROJECT-3: Extend the Inlinear Parser
######################################

.. |PROJECT-3| issue:: Extend the Inlinear Parser
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :dependencies: PROJECT-2
   :status: done

   A brief description for CLI output. The goal of a PROJECT
   ticket is to group a series of feature tickets.

Description
***********

The goal of this project is to add support for additional
inlinear elements to meet the requirements of :issue:`PHASE-1`.

These requirements include strong text, emphatic text, and
literal text.

Tickets
=======

.. tickets::

   PARSER-7
   PARSER-8
   PARSER-9


