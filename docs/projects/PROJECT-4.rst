
###########################################
PROJECT-4: Inlinear reStructuredText Tools
###########################################

.. |PROJECT-4| issue:: Extend the Inlinear Parser
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement CLI tools to lex and parse an arbitrary string.

Description
***********

The goal of this project is to build out command-line tools to
run the inlinear lexer and parser on an arbitrary string.

Support JSON and YAML output by default.

Tickets
=======

.. tickets::

   PARSER-10
   PARSER-11


