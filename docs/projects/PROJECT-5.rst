
###############################################
PROJECT-5: Basic Linear reStructuredText Lexer
###############################################

.. |PROJECT-5| issue:: Basic Linear reStructuredText Lexer
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement the basic linear reStructuredText lexer.


Description
***********

The goal of this project is to implement the basics of the
linear reStructuredText lexer, including support for tokens,
their initialization and analysis, as well as the lexer and all
relevant tests.

Tickets
=======

.. tickets::

   PARSER-12
   PARSER-13
   PARSER-14


