
###############################################
PROJECT-6: Basic Linear reStructuredText Parser
###############################################

.. |PROJECT-5| issue:: Basic Linear reStructuredText Lexer
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: progress

   Implement the basic linear reStructuredText lexer.


Description
***********

The goal of this project is to implement the basics of the
linear reStructuredText parser, including support for rendering
sections and paragraphs.

Tickets
=======

.. tickets::

   PARSER-15
   PARSER-16
   PARSER-17
   PARSER-18


