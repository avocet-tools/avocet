
##########################
PROJECT-7: Render Support
##########################

.. |PROJECT-7| issue:: Render Support
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: progress

   Update the AST to support rendering to stdout, HTML, and
   LaTeX.

Description
***********
As part of :document:`DESCR-5`, implement support for string
(stdout), HTML, and LaTeX rendering of the AST. Update the
intermediary parser tools to support rendering to the given
outputs.

Tickets
=======

.. tickets::

   PARSER-20
   PARSER-24


