
#################################
PROJECT-8: Implement Linear Tools
#################################

.. |PROJECT-8| issue:: Linear Tools
   :reporter: kennethpjdyer
   :assignee: kennethpjdyer
   :status: done

   Implement intermediary tools for the linear parser.

Description
***********

To facilitate development, implement two tools to evaluate the
linear lexer and parser during development.

Tickets
=======

.. tickets::

   PARSER-21

