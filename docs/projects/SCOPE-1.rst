
##########################################
SCOPE-1: Standard reStructuredText Parser
##########################################

.. |SCOPE-1| document:: Standard RST Parser
   :status: done
   :author: kennethpjdyer

   Implement a reStrucutedText lexer and parser able to handle
   basic and advanced nodes, to the exclusion of directives and
   interpreted text. 

Summary
*******

The standard parser implement support for most, but not all,
features of the reStructuredText specification. This scope
outlines the targets of this scope, how it breaks down into
phases, and what will be pushed off to a future scope. 

This document scopes work in :doc:`INIT-1`.


Motivation
**********

Avocet Tools read source content from files stored in a
configured or working directory. The "standard" format of
reStructuredText is not specification complete.

Our goal here is to cover the most common features of the
language and prepare for more complex future work on directives
and interpreted text.

Work
*******

Documents
=========

.. documents::

   INIT-1
   DESIGN-1
   DESCR-1
   DESCR-2
   DESCR-3
   DESCR-4

Goals
=====

The goals in this scope break into two categories: 

- Goals that cover the reStructuredText language and the
  features the parser supports.

- Goals that cover Avocet Tools and the intermediate tools
  introduced by this scope.

reStructuredText Language
-------------------------

- Structural Features
  - [x] Sections
  - [x] Section titles
- Block Features
  - [x] Paragraphs

- Element Features
  - [x] Inline String
  - [x] Strong Text
  - [x] Emphatic Text
  - [x] Literal Text

Tools
-----

- [x] Linear Lexer
- [x] Linear Parser
- [x] Inlinear Lexer
- [x] Inlinear Parser


Non-Goals
=========

- Directives
- Roles
- Targets
- Substitution
- Interpreted Text
- Links
- Lists
  - Itemized
  - Enumerated
  - Checklists
  - Definition
  - Field Lists
- Footnotes
- Endnotes
 
Changes
*******

User Experience
===============

At the completion of this scope, the user will have four tools
to analyze strings of reStructuredText provided from the
command-line.

Configuration
=============

Configuration Changes:

* :setting:`avocet.format`


Metrics
*******

The work in this scope will be complete when the user is able to
lex and parse a string from the command-line and view the JSON
output to evaluate whether the AST meets their expectations.

.. tickets::

   PHASE-1
   PHASE-2

