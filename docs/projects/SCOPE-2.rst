
##################################
SCOPE-2: Intermediary RST Features
##################################

.. |SCOPE-2| document: Intermediary RST Features
   :type: scope
   :status: done
   :author: kennethpjdyer
   :dependencies: SCOPE-1

   Extend support for additional reStructuredText language
   features, including targets, substitution, lists, and notes.

Summary
*******

The goal of this scope is to extend language support for the
reStructuredText specification, including a few Avocet-specific
features such as tags and checklists.

Motivation
**********

As of :release:`0.2.0`, the Avocet reStructuredText parser can
handle basic language features including sections, paragraphs,
as well as strong, emphatic, and literal text.

For this project, we want to expand support to cover targets,
substitutions, lists, notes, and comments.

The use of targets and notes will require the introduce of a
parent document to the linear parser to handle high-level target
references.

Support for notes will require the introduce a validation
process to check whether a ``FOOT`` element key corresponds to
a ``FOOTNOTE`` block key. For checklists, the validation will
need to match arbitrary characters with the supported
configuration.

Additionally, support for the inline note elements will require
the introduction of a resolver to set incremental text values on
the Element.Text field.


Targets
*******

Goals
=====

List of goals we aim to achieve in this project.

- [x] Targets
- [x] Lists
  - [x] Itemized
  - [x] Enumerated
  - [x] Checklists
  - [x] Definition
  - [x] Field Lists
- [x] Notes
  - [x] Footnotes
  - [x] Margin-notes
- [x] Comments
 
Non-Goals
=========

- Directives
- Roles
- Interpreted Text
- Links
- Substitution

Changes
*******

User Experience
===============

The goals of this project revolve around extending the parser to
support additional language features. Avocet reStructuredText
features that were available in the previous release remain
available in this release.

Configuration
=============

* The configuration file will support checklist configuration.





