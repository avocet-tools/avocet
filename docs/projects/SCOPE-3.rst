
#########################################
SCOPE-3: Advanced reStructuredText Parser
#########################################

.. |SCOPE-3| document:: Advanced reStructuredText Parser 
   :author: kennethpjdyer

   Implement advanced reStructuredText parser features, such as
   directives, roles, links, and interpreted text.


Summary
*******

The final stage of the base implementation of the parser is to
introduce support for advanced features, including directives,
roles, interpreted text, links, and substitution.

Motivation
**********

The competitive advantage of reStructuredText over Markdown lies
in roles and directives.  Specifically, the ability of users to
implement custom blocks and elements.

The goal of this scope is to provide the base implementation of
roles, directives, and interpretive text in general.


Targets
*******

Goals
=====

- [ ] Directives
  - [ ] Key Default Directives
  - [ ] Key Domain Directives
  - [ ] Domain Directives
  - [ ] Default Directives
- [ ] Directive Processing
  - [ ] Preserved Content
  - [ ] Parsed Content
  - [ ] Parsed Argument
- [ ] Roles
- [ ] Interpreted Text
- [ ] Links
- [ ] Substitution


Non-Goals
=========

- Feature Sets
  - Docutils
  - Sphinx
  - Snooty
  - Avocet
- Domain specifications
  - Spec
  - Go
  - Python
- Language Sets
- Role validation and processing


