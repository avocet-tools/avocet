

#############
``arstInlex``
#############

Description
***********

.. program:: arstInlex

   Performs inlinear lexical analysis on the given
   reStructuredText string.

   It is primarily used during development to observe what the
   lexer passes to the parser when given an arbitrary string. 

   .. versionadded:: 0.2.0

   .. include:: /includes/options/format

Syntax
******

.. code-block:: text

   arstInlex [--format FORMAT] TEXT

Examples
********

Output to JSON
==============


To output JSON, run the following command:

.. io-code-block::

   .. input::

      arstInlex Text | jq

   .. output:: json

      [
         {
           "type": 3,
           "name": "TEXT_TOKEN",
           "file": "STDIN",
           "line": 1,
           "column": 1,
           "position": 0,
           "text": "Text",
           "size": 0
         },
         {
           "type": 1,
           "name": "END_OF_LINE_TOKEN",
           "file": "STDIN",
           "line": 1,
           "column": 5,
           "position": 4,
           "text": "",
           "size": 0
         }
      ]

Output to YAML
==============

To output to YAML, run :program:`arstInlex` with the
``--format`` option set to ``yaml``:

.. io-code-block::

   .. input::

      arstInlex --format yaml Test

   .. output:: yaml

      - type: 3
        name: TEXT_TOKEN
        file: STDIN
        line: 1
        column: 1
        position: 0
        text: Test
        size: 0
      - type: 1
        name: END_OF_LINE_TOKEN
        file: STDIN
        line: 1
        column: 5
        position: 4
        text: ""
        size: 0


