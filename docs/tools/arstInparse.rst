
###############
``arstInparse``
###############

Description
***********

.. program:: arstInparse

   Intermediary tool used to evaluate the results of the inlinear
   parse of a reStructuredText string.

   .. versionadded:: 0.2.0

   .. include:: /includes/options/format

Syntax
******

.. code-block:: text

   arstInlex [--format FORMAT] TEXT

Examples
********

Parse reStructuredText to JSON
==============================

To parse a reStructuredText string to JSON:

.. io-code-block::

   .. input::

      arstInparse --format json Text | jq

   .. output:: json

      [
        {
          "type": 1,
          "type_name": "INLINE_TEXT",
          "file": "STDIN",
          "line": 1,
          "column": 1,
          "text": "Text"
        }
      ]


Parse reStructuredText to YAML 
==============================

To parse a reStructuredText string to YAML:

.. io-code-block::

   .. input::

      arstInparse --format yaml Text 

   .. output:: yaml

      - type: 1
        type_name: INLINE_TEXT
        file: STDIN
        line: 1
        column: 1
        text: Text


Parse reStructuredText to reStructuredText
==========================================

To parse a reStructuredText string to YAML:

.. io-code-block::

   .. input::

      arstInparse --format rst 'Text with *emphasis* and **strong** formatting'

   .. output::

      Text with *emphasis* and **strong** formatting


Parse reStructuredText to Markdown
==================================

To parse a reStructuredText string to YAML:

.. io-code-block::

   .. input::

      arstInparse --format md 'Text with *emphasis* and **strong** formatting'

   .. output::

      Text with *emphasis* and **strong** formatting


Parse reStructuredText to HTML
==============================

To parse a reStructuredText string to YAML:

.. io-code-block::

   .. input::

      arstInparse --format md 'Text with *emphasis* and **strong** formatting'

   .. output::

      Text with <em>emphasis</em> and <strong>strong</strong> formatting


Parse reStructuredText to LaTeX
================================

To parse a reStructuredText string to YAML:

.. io-code-block::

   .. input::

      arstInparse --format md 'Text with *emphasis* and **strong** formatting'

   .. output::

      Text with \emph{emphasis} and \textbf{strong} formatting

Parse reStructuredText to Stdout
================================

To parse a reStructuredText string to YAML:

.. io-code-block::

   .. input::

      arstInparse 'Text with *emphasis* and **strong** formatting'

   .. output::

      Text with emphasis and strong formatting










