package avocet

import (
	rstLexer "gitlab.com/avocet-tools/avocet/parsers/rst/inline/lexer"
	rstParser "gitlab.com/avocet-tools/avocet/parsers/rst/inline/parser"
)

// RunInlineLexer is run from the command-line by intermediary
// tools. It performs lexical analysis on an arbitrary string
// and then returns the tokens from an inlinear lex.
func RunInlineLexer(rdr string, args []string) {

	lgr.Infof("Called the %s inlinear lexer", rdr)
	switch rdr {
	case "rst":
		rstLexer.RunCommand(args)
	default:
		lgr.Fatalf("invalid read format: %s", rdr)
	}

}

// RunInlineParser is run from the command-line by intermediary
// tools. It performs semantic analysis on an arbitrary string
// and then returns the ast.Elements found in the parse.
func RunInlineParser(rdr string, args []string) {
	lgr.Infof("Called the %s inlinear parser", rdr)
	switch rdr {
	case "rst":
		rstParser.RunCommand(args)
	default:
		lgr.Fatalf("invalid read format: %s", rdr)
	}
}
