package avocet

import (
	rstLexer "gitlab.com/avocet-tools/avocet/parsers/rst/line/lexer"
	rstParser "gitlab.com/avocet-tools/avocet/parsers/rst/line/parser"
)

// RunLineLexer takes a reader value and an argument string
// slice and performs linear lexical analysis on the text. It
// prints results to stdout in the specified --format.
func RunLineLexer(rdr string, args []string) {
	lgr.Infof("Called the %s linear lexer", rdr)
	switch rdr {
	case "rst":
		rstLexer.RunCommand(args)
	default:
		lgr.Fatalf("Invalid reader format: %s", rdr)
	}
}

// RunLineParser takes a reader value and an argument string
// slice and performs linear parse on the text. It prints the
// results to stdout in the specified --format.
func RunLineParser(rdr string, args []string) {
	lgr.Infof("Called the %s linear parser", rdr)
	switch rdr {
	case "rst":
		rstParser.RunCommand(args)
	default:
		lgr.Fatalf("Invalid reader format: %s", rdr)
	}
}
