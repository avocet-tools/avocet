// Package lexer provides structs, functions, and methods used
// in the inlinear lexical analysis of a reStructuredText
// string.
package lexer

import (
	"fmt"
	"os"
	"strings"

	"github.com/charmbracelet/log"

	"gitlab.com/avocet-tools/avocet/parsers/rst/inline/token"
)

// Lexer struct is used to perform inlinear lexical analysis on
// a reStructuredText string.
type Lexer struct {
	in   []rune
	size int
	pos  int

	file string
	line int
	col  int

	lgr *log.Logger

	prime   *token.Token
	secunde *token.Token
	terte   *token.Token
	quarte  *token.Token
	quinte  *token.Token

	prefix string
}

// New takes a text string, a filename, and a line number and
// returns a *Lexer ready to perform inlinear lexical analysis
// of the given reStructuredText string.
//
// This function is mainly used internally for testing and by
// CLI tools that examine the inlinear lexical analysis process.
func New(text, file string, line int) *Lexer {
	rs := []rune(text)
	l := &Lexer{
		in:   rs,
		size: len(rs),
		file: file,
		line: line,
		col:  1,
		lgr: log.NewWithOptions(
			os.Stderr,
			log.Options{
				ReportTimestamp: false,
				Level:           log.DebugLevel,
			},
		),
		prefix: fmt.Sprintf("arstInlex %s", file),
	}
	l.lgr.SetPrefix(l.prefix)
	l.lgr.Debug("lexer initialized")

	l.initRead()

	return l

}

// NewArr takes a string slice, a filename, and a line number
// and returns a *Lexer ready to perform inlinear lexical
// analysis of the given reStructuredText string.
//
// It manages this by joining the string slice on '\n'
// characters. This function is mainly used internally with the
// linear parser to send a group of text lines through to the
// inlinear parser.
func NewArr(text []string, file string, line int) *Lexer {
	return New(strings.Join(text, "\n"), file, line)
}
