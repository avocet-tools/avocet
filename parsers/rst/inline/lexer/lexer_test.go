package lexer

import (
	"testing"

	"gitlab.com/avocet-tools/avocet/parsers/rst/inline/token"
)

func Test_lexer(t *testing.T) {
	data := []struct {
		r string
		s string
		t token.Type
	}{
		{"Text", "Text", token.TEXT},
		{"  ", " ", token.SPACE},
		{"*", "*", token.STAR},
		{"**", "**", token.DSTAR},
		{"``", "``", token.DTICK},
		{"[1]_", "", token.FOOT},
		{"[1]__", "", token.MARGIN},
	}

	for pos, datum := range data {
		l := New(datum.r, "lexer_test.go", pos)
		size := len([]rune(datum.r))
		if l.size != size {
			t.Errorf("lexer set wrong size: %d != %d", size, l.size)
			continue
		}

		tok := l.Next()

		if tok.Type != datum.t {
			t.Errorf("lexer returned wrong type: %s != %s", tok.Type, datum.t)
			continue
		}

		if tok.Text != datum.s {
			t.Errorf("lexer set wrong text on token: %q != %q", tok.Text, datum.s)
			continue
		}
		tok = l.Next()
		if tok.Type != token.EOL {
			t.Errorf("lexer failed to set EOL: %s != %s", tok.Type, token.EOL)
		}
	}

}
