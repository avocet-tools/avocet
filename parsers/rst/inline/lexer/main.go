package lexer

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet/parsers/rst/inline/token"
	"gopkg.in/yaml.v3"
)

const prefix = "arstInlex"

// RunCommand performs inlinear lexical analysis on an arbitrary
// string. It is called from the command-line to test
// intermediary stages of the parse by presenting the user with
// the tokens the lexer passes to the inlinear parser for a
// given string of inline reStructuredText.
func RunCommand(args []string) {
	// Collect Tokens
	l := New(strings.Join(args, " "), "STDIN", 1)

	toks := []*token.Token{}
	tok := l.Next()

	for tok.Type != token.EOL {
		toks = append(toks, tok)
		tok = l.Next()
	}
	toks = append(toks, tok)

	// Reset the Prefix
	l.lgr.SetPrefix(l.prefix)
	l.lgr.Debug("Lexing complete.")

	format := viper.GetString("avocet.format")
	switch format {
	case "json":
		formatJSON(l, toks)
	case "yaml":
		formatYAML(l, toks)
	default:
		l.lgr.Fatalf("Unknown format %q", format)
	}
}

func formatJSON(l *Lexer, toks []*token.Token) {
	l.lgr.Debugf("Formatting tokens to JSON")

	data, err := json.Marshal(toks)
	if err != nil {
		l.lgr.Fatalf("Unable to marshal tokens to JSON: %s", err)
	}
	fmt.Println(string(data))
}

func formatYAML(l *Lexer, toks []*token.Token) {
	l.lgr.Debugf("Formatting tokens to YAML")
	data, err := yaml.Marshal(toks)
	if err != nil {
		l.lgr.Fatalf("Unable to marshal tokens to YAML: %s", err)
	}
	fmt.Println(string(data))
}
