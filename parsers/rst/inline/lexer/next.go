package lexer

import "gitlab.com/avocet-tools/avocet/parsers/rst/inline/token"

// Next method returns the next *token.Token in the sequence.
// When it reaches the end of the input it begins to returns
// tokens with the token.EOL type.
func (l *Lexer) Next() *token.Token {
	tok := l.prime

	l.analyze(tok)
	l.read()

	return tok
}

func (l *Lexer) get(pos int) rune {
	if pos < l.size {
		return l.in[pos]
	}
	return 0
}

func (l *Lexer) getNext() *token.Token {
	tok := token.New(l.file, l.line, l.col, l.pos, l.get(l.pos))
	l.pos++

	// Increment Line Number
	if tok.Rune == '\n' {
		l.line++
		l.col = 0
	}
	l.col++

	return tok
}

func (l *Lexer) initRead() {
	l.prime = l.getNext()
	l.secunde = l.getNext()
	l.terte = l.getNext()
	l.quarte = l.getNext()
	l.quinte = l.getNext()
}

func (l *Lexer) read() {
	l.prime = l.secunde
	l.secunde = l.terte
	l.terte = l.quarte
	l.quarte = l.quinte
	l.quinte = l.getNext()
}

func (l *Lexer) analyze(tok *token.Token) {
	switch tok.Type {
	case token.TEXT:
		rs := []rune{tok.Rune}
		for l.secunde.Type == token.TEXT {
			rs = append(rs, l.secunde.Rune)
			l.read()
		}
		tok.Text = string(rs)
	case token.STAR:
		if l.secunde.Type == token.STAR {
			tok.Set(token.DSTAR)
			tok.Text = "**"
			l.read()
			return
		}
		tok.Set(token.STAR)
	case token.TICK:
		if l.secunde.Type == token.TICK {
			tok.Set(token.DTICK)
			tok.Text = "``"
			l.read()
			return
		}
	case token.SPACE:
		tok.Text = " "
		for l.secunde.Type == token.SPACE {
			l.read()
		}
	case token.OBRACK:
		key := []rune{}
		for l.secunde.Type == token.TEXT {
			key = append(key, l.secunde.Rune)
			l.read()
		}
		tok.Key = string(key)

		if l.secunde.Type == token.CBRACK && l.terte.Type == token.SCORE {
			if l.quarte.Type == token.SCORE {
				tok.Set(token.MARGIN)
				l.read()
			} else {
				tok.Set(token.FOOT)
			}
			l.read()
			l.read()
			return
		}
		tok.Set(token.TEXT)
	}
}
