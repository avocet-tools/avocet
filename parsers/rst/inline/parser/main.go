package parser

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet/ast"
	"gitlab.com/avocet-tools/avocet/render"
	"gopkg.in/yaml.v3"
)

// RunCommand performs an inlinear parse on a reStructuredText
// string. It is used to parse arbitrary strings from the
// command-line.
func RunCommand(args []string) {
	p := NewArr(args, "STDIN", 1)
	es := p.Parse()

	format := viper.GetString("avocet.format")
	switch format {
	case "stdout":
		r := render.Stdout()
		fmt.Println(r.RenderElements(es))
	case "latex":
		r := render.TeX()
		fmt.Println(r.RenderElements(es))
	case "rst":
		r := render.RST()
		fmt.Println(r.RenderElements(es))
	case "md":
		r := render.MD()
		fmt.Println(r.RenderElements(es))
	case "html":
		r := render.HTML()
		fmt.Println(r.RenderElements(es))
	case "json":
		formatJSON(p, es)
	case "yaml":
		formatYAML(p, es)
	default:
		p.lgr.Fatalf("invalid output format: %s", format)
	}
}

func formatJSON(p *Parser, es []*ast.Element) {
	data, err := json.Marshal(es)
	if err != nil {
		p.lgr.Fatalf("error marshaling to JSON: %s", err)
	}
	fmt.Fprintln(os.Stderr, "")
	fmt.Println(string(data))
}

func formatYAML(p *Parser, es []*ast.Element) {
	data, err := yaml.Marshal(es)
	if err != nil {
		p.lgr.Fatalf("error marshaling to YAML: %s", err)
	}
	fmt.Fprintln(os.Stderr, "")
	fmt.Println(string(data))
}
