package parser

import (
	"fmt"
	"os"

	"github.com/charmbracelet/log"

	"gitlab.com/avocet-tools/avocet/ast"
	"gitlab.com/avocet-tools/avocet/parsers/rst/inline/lexer"
	"gitlab.com/avocet-tools/avocet/parsers/rst/inline/token"
)

// Parser is used to perform inlinear parsing of a
// reSturcturedText string.
type Parser struct {
	file string
	es   []*ast.Element

	l      *lexer.Lexer
	lgr    *log.Logger
	prefix string

	prime   *token.Token
	secunde *token.Token
	terte   *token.Token
	quarte  *token.Token
	quinte  *token.Token
}

// New takes a text string, a filename, and a line number and
// returns a *Parser ready to perform an inlinear parse.
func New(text string, file string, line int) *Parser {

	p := &Parser{
		l: lexer.New(text, file, line),
	}

	p.start()

	return p

}

// NewArr takes a string slice, a filename, and a line number
// and returns a *Parser ready to perform an inlinear parse.
func NewArr(lines []string, file string, line int) *Parser {
	p := &Parser{
		l:    lexer.NewArr(lines, file, line),
		file: file,
	}
	p.start()

	return p
}

func (p *Parser) start() {
	p.es = []*ast.Element{}
	p.prefix = fmt.Sprintf("arstInparse %s", p.file)
	p.lgr = log.NewWithOptions(
		os.Stderr,
		log.Options{
			ReportTimestamp: false,
			Level:           log.DebugLevel,
			Prefix:          p.prefix,
		},
	)
	p.lgr.Debug("Lex initial tokens")

	p.prime = p.l.Next()
	p.secunde = p.l.Next()
	p.terte = p.l.Next()
	p.quarte = p.l.Next()
	p.quinte = p.l.Next()
}
