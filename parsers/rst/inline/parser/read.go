package parser

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
	"gitlab.com/avocet-tools/avocet/parsers/rst/inline/token"
)

// Parse runs the inlinear parser and returns an array of
// *ast.Elements.
func (p *Parser) Parse() []*ast.Element {
	p.lgr.Debug("Parse elements")

	for p.prime.Type != token.EOL {
		p.analyze()
		p.read()
	}
	p.lgr.SetPrefix(p.prefix)
	p.lgr.Debugf("Inlinear parse complete")

	return p.es
}

func (p *Parser) read() {
	p.prime = p.secunde
	p.secunde = p.terte
	p.terte = p.quarte
	p.quarte = p.quinte
	p.quinte = p.l.Next()
}

func (p *Parser) analyze() {
	switch p.prime.Type {
	case token.TEXT, token.SPACE:
		p.parseText()
	case token.DSTAR:
		p.parseBound(token.DSTAR, ast.STRONG)
	case token.STAR:
		p.parseBound(token.STAR, ast.EMPHASIS)
	case token.DTICK:
		p.parseBound(token.DTICK, ast.LITERAL)
	case token.FOOT:
		p.parseNote(ast.FOOT)
	case token.MARGIN:
		p.parseNote(ast.MARGIN)
	}
}

func (p *Parser) add(e *ast.Element) {
	p.es = append(p.es, e)
}

func (p *Parser) parseText() {
	e := ast.NewElement(p.prime.File, p.prime.Line, p.prime.Col)
	e.Set(ast.TEXT)

	text := []string{p.prime.Text}
	for p.secunde.Type == token.TEXT || p.secunde.Type == token.SPACE {
		text = append(text, p.secunde.Text)
		p.read()
	}

	e.Text = strings.Join(text, "")
	p.add(e)

}

func (p *Parser) parseBound(closer token.Type, etype ast.EType) {
	e := ast.NewElement(p.prime.File, p.prime.Line, p.prime.Col)
	e.Set(etype)
	text := []string{}
	closeText := p.prime.Text

	// Read to End
	for p.secunde.Type != closer && p.prime.Type != token.EOL {
		text = append(text, p.secunde.Text)
		p.read()
	}

	// If Next is EOL, send error
	if p.secunde.Type == token.EOL {
		p.lgr.SetPrefix(fmt.Sprintf("%s:%d.%d", p.prefix, e.Line, e.Col))
		p.lgr.Errorf("unclosed inline element %q", closeText)
		p.lgr.SetPrefix(p.prefix)
		return
	}
	p.read()

	e.Text = strings.Join(text, "")
	p.add(e)
}

func (p *Parser) parseNote(etype ast.EType) {
	e := ast.NewElement(p.prime.File, p.prime.Line, p.prime.Col)
	e.Set(etype)
	e.Key = p.prime.Key
	e.Text = p.prime.Text
	p.add(e)

}
