package parser

import (
	"testing"

	"gitlab.com/avocet-tools/avocet/ast"
)

func Test_parse(t *testing.T) {
	data := []struct {
		r string
		s string
		t ast.EType
		k string
	}{
		{"Text", "Text", ast.TEXT, ""},
		{"**Text**", "Text", ast.STRONG, ""},
		{"*Text*", "Text", ast.EMPHASIS, ""},
		{"``Text``", "Text", ast.LITERAL, ""},
		{"[1]_", "", ast.FOOT, "1"},
		{"[1]__", "", ast.MARGIN, "1"},
	}

	for _, datum := range data {
		p := New(datum.r, "read_test.go", 1)
		es := p.Parse()

		size := len(es)

		if size < 1 {
			t.Errorf("parser returned wrong values: %d < 1", size)
			continue
		}
		e := es[0]
		if e.Type != datum.t {
			t.Errorf("parser returned element of wrong type: %s != %s", e.Type, datum.t)
			continue
		}

		if e.Text != datum.s {
			t.Errorf("parser set wrong text: %q != %q", e.Text, datum.s)
			continue
		}

		if e.Key != datum.k {
			t.Errorf("parser set wrong key: %q != %q", e.Key, datum.k)
			continue
		}
	}
}
