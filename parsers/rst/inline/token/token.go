// Package token provides structs, types, and functions used in
// the inlinear lexical analysis of a reStructuredText string.
package token

// Token struct represents a sequence of inline text taken from
// a reStructuredText string. It begins a single rune, but the
// lexer may choose to add to it sequential runes of a similar
// type.
type Token struct {
	Type Type   `json:"type" yaml:"type"`
	Name string `json:"name" yaml:"name"`

	File string `json:"file" yaml:"file"`
	Line int    `json:"line" yaml:"line"`
	Col  int    `json:"column" yaml:"column"`
	Pos  int    `json:"position" yaml:"position"`

	Rune rune   `json:"-" yaml:"-"`
	Text string `json:"text" yaml:"text"`
	Size int    `json:"size" yaml:"size"`

	Key string `json:"key,omitempty" yaml:"key,omitempty"`
}

// New takes a filename, line and column numbers, a position,
// and a rune and initializes a *Token, which it returns to the
// caller. The function performs the initial lexical analysis,
// classifying the *Token for single-rune types.
//
// Note: the lexer may alter this initial type based on
// subsequent tokens. In the case of SPACE and TEXT tokens it
// may also eat subsequent runes, appending them to the
// Token.Text attribute.
func New(file string, line, col, pos int, r rune) *Token {
	tok := &Token{
		File: file,
		Line: line,
		Col:  col,
		Pos:  pos,
	}

	// Set Token for single-character tokens.
	tok.Rune = r
	switch r {
	case 0:
		tok.Set(EOL)
	case ' ', '\t', '\n', '\r':
		tok.Set(SPACE)
	case '*':
		tok.Set(STAR)
		tok.Text = "*"
	case '`':
		tok.Set(TICK)
		tok.Text = "`"
	case '[':
		tok.Set(OBRACK)
	case ']':
		tok.Set(CBRACK)
	case '_':
		tok.Set(SCORE)
	default:
		tok.Set(TEXT)
	}

	return tok
}

// Set takes a Type argument and sets it on the Token.Type. For
// the convenience of CLI outputs, it also sets the string
// representation of the Type on the Token.Name attribute.
func (tok *Token) Set(t Type) {
	tok.Type = t
	tok.Name = t.String()
}

// Append takes an array of runes drawn from other tokens and
// updates the Token.Text and Token.Size attributes with their
// content.
func (tok *Token) Append(rs []rune) {
	rs = append([]rune{tok.Rune}, rs...)
	tok.Size = len(rs)
	tok.Text = string(rs)
}
