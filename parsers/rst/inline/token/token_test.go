package token

import "testing"

func Test_token(t *testing.T) {
	data := []struct {
		r rune
		t Type
	}{
		{'T', TEXT},
		{0, EOL},
		{' ', SPACE},
		{'\t', SPACE},
		{'\n', SPACE},
		{'\r', SPACE},
		{'*', STAR},
		{'`', TICK},
		{'[', OBRACK},
		{']', CBRACK},
		{'_', SCORE},
	}

	for pos, datum := range data {
		tok := New("token_test.go", 1, pos, pos, datum.r)

		if tok.Type != datum.t {
			t.Errorf("token initialized to wrong type: %s != %s", tok.Type, datum.t)
		}
	}

}
