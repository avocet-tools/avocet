package token

import "fmt"

// Type is an integer constant used to classify the contents of
// a Token by the lexer.
type Type int

const (

	// UNSET indicates a Token that has not been processed by the
	// lexer. The parser should not receive UNSET tokens and
	// their presence at that stage indicates a wider error.
	UNSET Type = iota

	// EOL indicates the end of a line and is used to tell the
	// inlinear parser that it is done with the input.
	EOL

	// SPACE indicates a whitespace character, such as a space, a
	// newline, or a tab. Note, the lexer converts all whitespace
	// characters into a single space and eats sequential
	// whitespace characters to remove anomalies, such as the use
	// of two spaces after a period.
	SPACE

	// TEXT indicates that the Token contains text characters,
	// which are not otherwise significant to the lexer. Note,
	// the lexer eats sequential TEXT tokens, so a string of
	// character is passed to the parser as a single TEXT token.
	TEXT

	// STAR indicates that the Token contains an asterisk.
	STAR

	// DSTAR indicates that the Token contains a double asterisk.
	DSTAR

	// DTICK indicates that the Token contains a double backtick.
	DTICK

	// TICK indicates the Token contains a single backtick.
	TICK

	// OBRACK indicates an open bracket
	OBRACK

	// CBRACK indicates a closed bracket
	CBRACK

	// SCORE indicates an underscore
	SCORE

	// FOOT indicates a footnote token.
	FOOT

	// MARGIN indicates a margin note.
	MARGIN
)

const (
	unsetType  = "UNSET_TOKEN"
	eolType    = "END_OF_LINE_TOKEN"
	spaceType  = "SPACE_TOKEN"
	textType   = "TEXT_TOKEN"
	starType   = "STAR_TOKEN"
	dstarType  = "DOUBLE_STAR_TOKEN"
	tickType   = "TICK_TOKEN"
	dtickType  = "DOUBLE_TICK_TOKEN"
	obrackType = "OPEN_BRACKET_TOKEN"
	cbrackType = "CLOSED_BRACKET_TOKEN"
	scoreType  = "SCORE_TOKEN"
	footType   = "FOOTNOTE_TOKEN"
	marginType = "MARGINALIA_TOKEN"
)

var typeString = map[Type]string{
	UNSET:  unsetType,
	EOL:    eolType,
	SPACE:  spaceType,
	TEXT:   textType,
	STAR:   starType,
	DSTAR:  dstarType,
	TICK:   tickType,
	DTICK:  dtickType,
	OBRACK: obrackType,
	CBRACK: cbrackType,
	SCORE:  scoreType,
	FOOT:   footType,
	MARGIN: marginType,
}

var stringType = map[string]Type{
	unsetType:  UNSET,
	eolType:    EOL,
	spaceType:  SPACE,
	textType:   TEXT,
	starType:   STAR,
	dstarType:  DSTAR,
	tickType:   TICK,
	dtickType:  DTICK,
	obrackType: OBRACK,
	cbrackType: CBRACK,
	scoreType:  SCORE,
	footType:   FOOT,
	marginType: MARGIN,
}

// String returns the string representation of a Type. If the
// Type is invalid, it returns an "UNKNOWN_TYPE_%d".
func (t Type) String() string {
	ret, ok := typeString[t]
	if ok {
		return ret
	}

	return fmt.Sprintf("UNKNOWN_TYPE_%d", t)
}

// StringToType takes the string representation of a Type and
// returns the corresponding Type. If the string does not match
// any known Type, it returns UNSET.
func StringToType(name string) Type {
	ret, ok := stringType[name]
	if ok {
		return ret
	}
	return UNSET
}
