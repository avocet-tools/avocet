package token

import "testing"

func getTypeData() []struct {
	t Type
	s string
} {
	data := []struct {
		t Type
		s string
	}{
		{UNSET, unsetType},
		{EOL, eolType},
		{SPACE, spaceType},
		{TEXT, textType},
		{STAR, starType},
		{DSTAR, dstarType},
		{TICK, tickType},
		{DTICK, dtickType},
		{OBRACK, obrackType},
		{CBRACK, cbrackType},
		{SCORE, scoreType},
		{FOOT, footType},
		{MARGIN, marginType},
	}
	return data
}

func Test_type_string(t *testing.T) {

	for _, datum := range getTypeData() {
		if datum.t.String() != datum.s {
			t.Errorf("wrong type string: %s != %s", datum.t, datum.s)
			continue
		}
		st := StringToType(datum.s)
		if st != datum.t {
			t.Errorf("wrong string type: %s != %s", st, datum.t)
		}

	}
}
