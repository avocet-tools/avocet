package lexer

import (
	"fmt"
	"os"
	"strings"

	"github.com/charmbracelet/log"

	"gitlab.com/avocet-tools/avocet/parsers/rst/line/token"
)

// Lexer struct provides struct to evaluate linear objects in a
// reStructuredText string.
type Lexer struct {
	in   []string
	toks []*token.Token
	pos  int
	file string
	size int

	tok *token.Token

	lgr    *log.Logger
	read   func()
	prefix string
}

// New takes text and a filename and returns a Lexer ready for
// lexical analysis.
func New(text, file string) *Lexer {
	l := &Lexer{
		file: file,
		in:   strings.Split(text, "\n"),
	}
	l.size = len(l.in)
	l.read = l.readString
	l.setLog()

	return l
}

func (l *Lexer) setLog() {
	l.prefix = fmt.Sprintf("arstLinlex %s", l.file)
	l.lgr = log.NewWithOptions(
		os.Stderr,
		log.Options{
			ReportTimestamp: false,
			Level:           log.DebugLevel,
			Prefix:          l.prefix,
		},
	)

}

// NewToks takes a slice of Tokens and a filename and returns a
// Lexer ready for lexical analysis. It is mainly used when
// subparsing indented content.
func NewToks(toks []*token.Token, file string) *Lexer {
	l := &Lexer{
		file: file,
		toks: toks,
		size: len(toks),
	}
	l.read = l.readToken
	l.setLog()

	return l
}
