package lexer

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet/parsers/rst/line/token"
	"gopkg.in/yaml.v3"
)

// RunCommand takes a string slice passed from the command-line.
// It then performs lexical analysis on the string and prints
// the result to stdout as JSON or YAML.
func RunCommand(args []string) {

	l := New(strings.Join(args, " "), "STDIN")
	toks := []*token.Token{}
	tok := l.Next()

	for tok.Type != token.EOF {
		toks = append(toks, tok)
		tok = l.Next()
	}
	toks = append(toks, l.Next())

	format := viper.GetString("avocet.format")
	l.lgr.Debugf("Formatting output to %s", format)
	fmt.Fprintln(os.Stderr, "\n ")
	switch format {
	case "json":
		formatJSON(l, toks)
	case "yaml":
		formatYAML(l, toks)
	default:
		l.lgr.Fatalf("invalid output format: %s", format)
	}
}

func formatJSON(l *Lexer, toks []*token.Token) {
	data, err := json.Marshal(toks)
	if err != nil {
		l.lgr.Fatalf("unable to render output to JSON: %s", err)
	}
	fmt.Println(string(data))
}

func formatYAML(l *Lexer, toks []*token.Token) {
	data, err := yaml.Marshal(toks)
	if err != nil {
		l.lgr.Fatalf("unable to render output to YAML: %s", err)
	}
	fmt.Println(string(data))
}
