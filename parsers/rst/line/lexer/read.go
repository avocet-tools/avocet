package lexer

import (
	"strings"
	"unicode"

	"gitlab.com/avocet-tools/avocet/parsers/rst/line/token"
)

func (l *Lexer) readString() {
	if l.pos < l.size {
		l.tok = token.New(l.in[l.pos], l.file, l.pos+1)
		l.pos++
		return
	}
	l.tok = token.New("", l.file, l.pos)
	l.tok.Set(token.EOF)
}

func (l *Lexer) readToken() {
	if l.pos < l.size {
		l.tok = l.toks[l.pos]
		l.pos++
		return
	}
	l.tok = token.New("", l.file, l.pos)
	l.tok.Set(token.EOF)
}

// Next readies the next token in the Lexer, performs lexical
// analysis and then returns the token to the parser.
func (l *Lexer) Next() *token.Token {
	l.read()
	tok := l.tok
	if tok.Type != token.EOF {
		l.analyze(tok)
	}

	return tok
}

func (l *Lexer) analyze(tok *token.Token) {
	switch tok.Get(0) {
	case 0:
		tok.Set(token.EMPTY)
		return
	case ' ':
		tok.CountWS()
		if tok.Indent == tok.Size {
			tok.Set(token.EMPTY)
			tok.Text = ""
			return
		}
		tok.Set(token.INDENT)
		return
	case '.':
		if l.isHeadline(tok) {
			tok.Set(token.HEAD)
			return
		} else if tok.Get(1) == '.' {
			l.lexDots(tok)
			return
		}
	case '#':
		if l.isHeadline(tok) {
			tok.Set(token.HEAD)
			return
		} else if tok.Get(1) == '.' && tok.Get(2) == ' ' {
			tok.DeHashList()
			return
		}
	case ':':
		if l.isHeadline(tok) {
			tok.Set(token.HEAD)
			return
		} else if l.isField(tok) {
			tok.Set(token.META)
			return
		}
	case '*', '+', '-':
		if l.isHeadline(tok) {
			tok.Set(token.HEAD)
			return
		} else if tok.Get(1) == ' ' {
			if tok.Get(2) == '[' && tok.Get(4) == ']' {
				tok.DeCList()
				return
			}
			tok.DeIList()
			return
		}
	case '!', '"', '$', '%', '&', '\'', '(', ')', ',', '/', ';',
		'<', '>', '=', '?', '@', '[', ']', '\\', '^', '_', '`', '{',
		'}', '~':
		if l.isHeadline(tok) {
			tok.Set(token.HEAD)
			return
		}
	}

	r := tok.Get(0)
	if unicode.IsNumber(r) || unicode.IsLetter(r) {
		if tok.Get(1) == '.' && tok.Get(2) == ' ' {
			tok.DeHashList()
			return
		}
	}

	tok.Set(token.TEXT)
}

func (l *Lexer) isField(tok *token.Token) bool {

	if tok.Size < 3 {
		return false
	}
	text := []rune{}
	for i, r := range tok.Runes[1:] {
		switch r {
		case ':':
			tok.Field = string(text)
			if tok.Size > i+2 {
				tok.Text = strings.TrimSpace(string(tok.Runes[i+2:]))
			} else {
				tok.Text = ""
			}
			return true
		case '-', '_':
		case ' ', '`', '\'', '"':
			return false
		}
		text = append(text, r)
	}

	return false
}

func (l *Lexer) isHeadline(tok *token.Token) bool {
	r := tok.Get(0)

	for _, sr := range tok.Runes {
		if r == sr {
			continue
		}
		if sr == ' ' {
			r = ' '
			continue
		}
		return false
	}
	return true
}

func (l *Lexer) lexDots(tok *token.Token) {
	tok.DeDot()
	switch tok.Get(0) {
	case '[':
		l.lexNote(tok)
		return
	case '_':
		l.lexTarget(tok)
		return
	}

	tok.Set(token.COMMENT)
}

func (l *Lexer) lexTarget(tok *token.Token) {
	key := []rune{}
	if tok.Get(1) == '`' {
		for i := 2; i < tok.Size; i++ {
			r := tok.Get(i)
			switch r {
			case '`':
				r = tok.Get(i + 1)
				if r == ':' {
					tok.Field = string(key)
					if tok.Size > i+2 {
						tok.Runes = tok.Runes[i+2:]
						tok.Text = strings.TrimSpace(string(tok.Runes))
						tok.Runes = []rune(tok.Text)
						tok.Size = len(tok.Runes)
						tok.Set(token.TARGET)
						return
					}
					tok.Text = ""
					tok.Runes = []rune{}
					tok.Size = 0
					tok.Set(token.TARGET)
				}
				return
			}
			key = append(key, r)
		}
	} else {
		for i := 1; i < tok.Size; i++ {
			r := tok.Get(i)
			switch r {
			case ' ':
				break
			case ':':
				tok.Field = string(key)
				tok.Set(token.TARGET)
				if tok.Size > i+1 {
					tok.Runes = tok.Runes[i+1:]
					tok.Text = strings.TrimSpace(string(tok.Runes))
					tok.Runes = []rune(tok.Text)
					tok.Size = len(tok.Runes)
					return
				}
				tok.Text = ""
				tok.Runes = []rune{}
				tok.Size = 0
				return
			}

			key = append(key, r)
		}
	}

	tok.Set(token.COMMENT)
}

func (l *Lexer) lexNote(tok *token.Token) {
	rkey := []rune{}
	toks := []*token.Token{}
	key := ""
	for i := 1; i < tok.Size; i++ {
		r := tok.Get(i)
		if r == ']' {
			key = string(rkey)
			if tok.Size > i+1 {
				tok.Runes = tok.Runes[i+1:]
				tok.Text = strings.TrimSpace(string(tok.Runes))
				tok.Runes = []rune(tok.Text)
				tok.Size = len(tok.Runes)
				toks = append(toks, tok)
				break
			}
		}
		rkey = append(rkey, r)
	}

	// Fail to COMMENT key unset
	if key == "" {
		tok.Set(token.COMMENT)
		return
	}
	tok.Field = key
	tok.Set(token.FNOTE)
}
