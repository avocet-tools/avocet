package lexer

import (
	"testing"

	"gitlab.com/avocet-tools/avocet/parsers/rst/line/token"
)

func Test_lexer(t *testing.T) {
	data := []struct {
		r string
		s string
		t token.Type
	}{
		{"Text", "Text", token.TEXT},
		{"", "", token.EMPTY},
		{" ", "", token.EMPTY},
		{"  Text", "  Text", token.INDENT},
		{"- Item", "Item", token.ILIST},
		{"+ Item", "Item", token.ILIST},
		{"* Item", "Item", token.ILIST},
		{"*   Item", "Item", token.ILIST},
		{"- [ ] Item", "Item", token.CLIST},
		{"+ [x] Item", "Item", token.CLIST},
		{"* [i] Item", "Item", token.CLIST},
		{"* [o] Item", "Item", token.CLIST},
		{"#. Item", "Item", token.ELIST},
		{"a. Item", "Item", token.ELIST},
		{"A. Item", "Item", token.ELIST},
		{"1. Item", "Item", token.ELIST},
		{":key:", "", token.META},
		{":key: value", "value", token.META},
		{".. comment", "comment", token.COMMENT},
		{".. [key] note", "note", token.FNOTE},
		{".. _key:", "", token.TARGET},
		{".. _key: example", "example", token.TARGET},
		{".. _`tick key`: example", "example", token.TARGET},
	}

	for _, datum := range data {
		l := New(datum.r, "read_test.go")
		tok := l.Next()

		if tok.Type != datum.t {
			t.Errorf("lexer set wrong type: %s != %s\n%s", tok.Type, datum.t, datum.r)
			continue
		}

		if tok.Text != datum.s {
			t.Errorf("lexer set wrong text for %s: %q != %q", tok.Type, tok.Text, datum.s)
			continue
		}
		tok = l.Next()
		if tok.Type != token.EOF {
			t.Errorf("lexer failed to set EOF token: %s != %s", tok.Type, token.EOF)
		}

	}
}

func Test_headline(t *testing.T) {
	rs := []rune{
		':', '*', '+', '-', '!', '"', '$', '%', '&', '\'', '(', ')',
		',', '/', ';', '<', '>', '=', '?', '@', '[', ']', '\\', '^',
		'_', '`', '{', '}', '~',
	}

	for _, r := range rs {
		text := string([]rune{r, r, r, '\n', r, r, r, ' '})
		l := New(text, "read_test.go")
		toks := []*token.Token{l.Next(), l.Next()}

		for _, tok := range toks {
			if tok.Type != token.HEAD {
				t.Errorf("lexer failed to identify headline %s: %s != %s", string(tok.Get(0)), tok.Type, token.HEAD)
			}
		}

	}

}
