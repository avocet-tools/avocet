package parser

import "gitlab.com/avocet-tools/avocet/ast"

// AddBlock adds a new block to the given Content.Data
func (p *Parser) AddBlock(b *ast.Block, con *ast.Content) {
	b.Idrefs = p.idrefs
	p.idrefs = []string{}

	con.Block(b)
}

// AddElement adds a new Element to the Content.Line
func (p *Parser) AddElement(e *ast.Element, con *ast.Content) {
	con.Element(e)
}

// AddSection adds a new section to the parent Content.Sections.
func (p *Parser) AddSection(sect *ast.Section) {
	p.lgr.Debugf("Literal: %s", string(sect.Level.Literal))

	// Find Level
	var parentLevel int
	level, ok := p.literalLevel[sect.Level.Literal]
	if !ok {
		level = len(p.levelSect)
		p.literalLevel[sect.Level.Literal] = level
		parentLevel = level - 1
	} else {
		parentLevel = level - 1
	}
	sect.Level.Local = level
	sect.Level.Global = level

	// Set Parent Content
	var con *ast.Content
	parent, ok := p.levelSect[parentLevel]
	if !ok {
		con = p.baseCon
	} else {
		con = parent
	}

	// Set Section Identity References
	sect.Idrefs = p.idrefs
	p.idrefs = []string{}
	con.Section(sect)
	p.levelSect[level] = sect.Content

	p.con = sect.Content
}
