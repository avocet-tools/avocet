package parser

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet/ast"
	"gitlab.com/avocet-tools/avocet/render"
	"gopkg.in/yaml.v3"
)

// RunCommand takes arguments from the command line and performs
// a linear parse on the text. It then prints the results of the
// parse to stdout in the specified --format.
func RunCommand(args []string) {
	p := New(strings.Join(args, " "), "STDIN")
	con, _ := p.Parse(ast.NewContent())

	format := viper.GetString("avocet.format")
	p.lgr.Debugf("Formatting ast.Content to %s", format)
	fmt.Fprintln(os.Stderr, "")
	switch format {
	case "stdout":
		r := render.Stdout()
		fmt.Println(r.RenderContent(con))
	case "rst":
		r := render.RST()
		fmt.Println(r.RenderContent(con))
	case "md":
		r := render.MD()
		fmt.Println(r.RenderContent(con))
	case "html":
		r := render.HTML()
		fmt.Println(r.RenderContent(con))
	case "latex", "tex":
		r := render.TeX()
		fmt.Println(r.RenderContent(con))
	case "json":
		formatJSON(con, p)
	case "yaml":
		formatYAML(con, p)
	default:
		p.lgr.Fatalf("invalid format %s", format)
	}
}

func formatJSON(con *ast.Content, p *Parser) {
	data, err := json.Marshal(con)
	if err != nil {
		p.lgr.Fatalf("unable to format JSON: %s", err)
	}
	fmt.Fprintln(os.Stderr, "")
	fmt.Println(string(data))
}

func formatYAML(con *ast.Content, p *Parser) {
	data, err := yaml.Marshal(con)
	if err != nil {
		p.lgr.Fatalf("unable to format YAML: %s", err)
	}
	fmt.Fprintln(os.Stderr, "")
	fmt.Println(string(data))
}
