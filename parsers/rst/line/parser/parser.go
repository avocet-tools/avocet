package parser

import (
	"fmt"
	"os"

	"github.com/charmbracelet/log"

	"gitlab.com/avocet-tools/avocet/ast"
	"gitlab.com/avocet-tools/avocet/parsers/rst/line/lexer"
	"gitlab.com/avocet-tools/avocet/parsers/rst/line/token"
)

// Parser struct is used to perform linear and inlinear parsing
// on reStructuredText strings.
type Parser struct {
	l    *lexer.Lexer
	file string

	prime   *token.Token
	secunde *token.Token
	terte   *token.Token

	idrefs  []string
	targets map[string]*ast.Element

	lgr    *log.Logger
	prefix string

	literalLevel map[rune]int
	levelSect    map[int]*ast.Content
	baseCon      *ast.Content
	con          *ast.Content

	parseField func(*ast.Content)
}

// New takes a reStructuredText string and a filename and
// returns a parser ready for analysis.
func New(text, file string) *Parser {
	p := &Parser{
		l:       lexer.New(text, file),
		file:    file,
		idrefs:  []string{},
		targets: make(map[string]*ast.Element),
	}
	p.parseField = p.parseDocumentField
	p.setup()

	return p
}

func newToks(toks []*token.Token, file string) *Parser {
	p := &Parser{
		l:      lexer.NewToks(toks, file),
		file:   file,
		idrefs: []string{},
	}
	p.setup()
	p.parseField = p.parseDocumentField

	return p

}

func (p *Parser) setup() {
	p.prime = p.l.Next()
	p.secunde = p.l.Next()
	p.terte = p.l.Next()
	p.literalLevel = make(map[rune]int)
	p.levelSect = make(map[int]*ast.Content)

	p.prefix = fmt.Sprintf("arstLinParse %s", p.file)

	p.lgr = log.NewWithOptions(
		os.Stderr,
		log.Options{
			ReportTimestamp: false,
			Level:           log.DebugLevel,
			Prefix:          p.prefix,
		},
	)
	p.lgr.Debugf("parser ready")
}

// Parse takes an ast.Content and parses the configured content,
// setting it on the Content.
func (p *Parser) Parse(con *ast.Content) (*ast.Content, map[string]*ast.Element) {
	p.baseCon = con
	p.con = con

	p.levelSect[0] = p.baseCon
	for p.prime.Type != token.EOF {
		p.analyze(p.con)
	}

	return con, p.targets
}

// Merge takes a subparser and merges target data into the main
// parser.
func (p *Parser) Merge(sp *Parser) {

	for k, e := range sp.targets {
		p.targets[k] = e
	}
}
