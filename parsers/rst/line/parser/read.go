package parser

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet/ast"
	inline "gitlab.com/avocet-tools/avocet/parsers/rst/inline/parser"
	"gitlab.com/avocet-tools/avocet/parsers/rst/line/token"
)

func (p *Parser) read() {
	p.prime = p.secunde
	p.secunde = p.terte
	p.terte = p.l.Next()
}

func (p *Parser) analyze(con *ast.Content) {

	switch p.prime.Type {
	case token.EOF:
		return
	case token.HEAD:
		if p.terte.Type == token.HEAD {
			p.parseOverHead(con)
			return
		}
		p.parseText(con)
	case token.ILIST, token.CLIST:
		p.parseIList(con)
	case token.ELIST:
		p.parseEList(con)
	case token.TEXT:
		if p.secunde.Type == token.HEAD {
			p.parseUnderHead(con)
			return
		}
		p.parseText(con)
	case token.META:
		p.parseField(con)
	case token.COMMENT:
		p.parseComment(con)
	case token.FNOTE:
		p.parseFootnote(con)
	case token.TARGET:
		p.parseTarget(con)
	}
	p.read()
}

func countToWhitespaceClose(tok *token.Token) int {
	return len([]rune(strings.TrimSpace(string(tok.Runes))))
}

func (p *Parser) parseOverHead(con *ast.Content) {
	over := countToWhitespaceClose(p.prime)
	under := countToWhitespaceClose(p.terte)
	title := countToWhitespaceClose(p.secunde)

	r := p.prime.Get(0)
	if over > under {
		p.lgr.SetPrefix(fmt.Sprintf("%s:%d", p.prefix, p.prime.Line))
		p.lgr.Warnf("Headline: overline is longer than underline: %s", string([]rune{r, r, r}))
		p.lgr.SetPrefix(p.prefix)
	} else if over < under {
		p.lgr.SetPrefix(fmt.Sprintf("%s:%d", p.prefix, p.prime.Line))
		p.lgr.Warnf("Headline: underline is longer than overline: %s", string([]rune{r, r, r}))
		p.lgr.SetPrefix(p.prefix)
	}

	if over < title {
		p.lgr.SetPrefix(fmt.Sprintf("%s:%d", p.prefix, p.prime.Line))
		p.lgr.Warnf("Headline: title text is longer than heading lines: %s", string([]rune{r, r, r}))
		p.lgr.SetPrefix(p.prefix)
	}
	p.parseHead(con, p.secunde, r)

	p.read()
	p.read()
	p.read()
}

func (p *Parser) parseUnderHead(con *ast.Content) {
	under := countToWhitespaceClose(p.secunde)
	title := countToWhitespaceClose(p.prime)

	r := p.secunde.Get(0)
	if under < title {
		p.lgr.SetPrefix(fmt.Sprintf("%s:%d", p.prefix, p.prime.Line))
		p.lgr.Warnf("Headline: title text is longer than heading lines: %s", string([]rune{r, r, r}))
		p.lgr.SetPrefix(p.prefix)
	}
	p.parseHead(con, p.prime, r)
	p.read()
	p.read()

}

func (p *Parser) parseHead(_ *ast.Content, title *token.Token, r rune) {
	sect := ast.NewSection(title.File, title.Line, r)

	// Parse Title
	sp := inline.New(strings.TrimSpace(title.Text), title.File, title.Line)
	sect.Content.Line = sp.Parse()

	p.parseField = p.parseSectionField

	p.AddSection(sect)
}

func (p *Parser) parseText(con *ast.Content) {

	if p.secunde.Type == token.INDENT && p.secunde.Indent > 2 {
		p.parseDefList(con)
		return
	}

	blk := ast.NewBlock(p.prime.File, p.prime.Line)
	blk.Set(ast.PARA)

	line := []string{p.prime.Text}
	for p.secunde.Type == token.TEXT {
		line = append(line, p.secunde.Text)
		p.read()
	}
	sp := inline.NewArr(line, blk.File, blk.Line)
	blk.Content.Line = sp.Parse()

	p.AddBlock(blk, con)
}

func (p *Parser) parseDefList(con *ast.Content) {
	list := ast.NewBlock(p.prime.File, p.prime.Line)
	list.Set(ast.DLIST)

	p.parseDefListItem(list.Content)

	for p.secunde.Type == token.TEXT && (p.terte.Type == token.INDENT && p.terte.Indent > 2) {
		p.read()
		p.parseDefListItem(list.Content)
	}

	p.AddBlock(list, con)
}

func (p *Parser) parseDefListItem(con *ast.Content) {
	li := ast.NewBlock(p.prime.File, p.prime.Line)
	li.Set(ast.DLISTITEM)
	p.parseTerm(li.Content)
	p.parseDefinition(li.Content)

	p.AddBlock(li, con)
}

func (p *Parser) parseTerm(con *ast.Content) {
	text := strings.Split(p.prime.Text, " : ")
	if len(text) > 1 {
		// TODO: Classifier Handling
		clses := []string{}
		for _, cls := range text[1:] {
			clses = append(clses, cls)
		}
		con.Meta["__CLASSIFIERS__"] = clses
	}

	term := ast.NewBlock(p.prime.File, p.prime.Line)
	term.Set(ast.TERM)

	sp := inline.New(text[0], p.prime.File, p.prime.Line)
	term.Content.Line = sp.Parse()

	p.AddBlock(term, con)
}

func (p *Parser) parseDefinition(con *ast.Content) {
	// Initialize Block
	blk := ast.NewBlock(p.prime.File, p.prime.Line)
	blk.Set(ast.DEF)

	// Read Content
	toks := []*token.Token{}
	for p.secunde.Type == token.EMPTY || (p.secunde.Type == token.INDENT && p.secunde.Indent > 2) {
		p.secunde.Dedent(3)
		toks = append(toks, p.secunde)
		p.read()
	}

	// Subparse Definition Content
	sp := newToks(toks, p.prime.File)
	sp.Parse(blk.Content)
	p.Merge(sp)

	p.AddBlock(blk, con)

}

func (p *Parser) parseIList(con *ast.Content) {
	blk := ast.NewBlock(p.prime.File, p.prime.Line)
	blk.Set(ast.ILIST)

	p.parseIListItem(blk.Content)

	for p.secunde.Type == token.ILIST || p.secunde.Type == token.CLIST {
		p.read()
		p.parseIListItem(blk.Content)
	}

	p.AddBlock(blk, con)
}

func (p *Parser) parseEList(con *ast.Content) {
	blk := ast.NewBlock(p.prime.File, p.prime.Line)
	blk.Set(ast.ELIST)

	p.parseEListItem(blk.Content)

	for p.secunde.Type == token.ELIST {
		p.read()
		p.parseEListItem(blk.Content)
	}

	p.AddBlock(blk, con)
}

func (p *Parser) parseIListItem(con *ast.Content) {
	toks := []*token.Token{p.prime}
	li := ast.NewBlock(p.prime.File, p.prime.Line)

	// Retrieve Key form Checklist Items
	li.Set(ast.ILISTITEM)
	li.Key = string(p.prime.Key)

	for p.secunde.Type == token.EMPTY || (p.secunde.Type == token.INDENT && p.secunde.Indent >= 2) {
		p.secunde.Dedent(2)
		toks = append(toks, p.secunde)
		p.read()
	}

	sp := newToks(toks, li.File)
	sp.Parse(li.Content)
	p.Merge(sp)

	p.AddBlock(li, con)
}

func (p *Parser) parseEListItem(con *ast.Content) {
	toks := []*token.Token{p.prime}
	li := ast.NewBlock(p.prime.File, p.prime.Line)
	li.Set(ast.ELISTITEM)

	for p.secunde.Type == token.EMPTY || (p.secunde.Type == token.INDENT && p.secunde.Indent >= 3) {
		p.secunde.Dedent(3)
		toks = append(toks, p.secunde)
		p.read()
	}
	sp := newToks(toks, li.File)
	sp.Parse(li.Content)
	p.Merge(sp)
	p.AddBlock(li, con)
}

func (p *Parser) parseSectionField(con *ast.Content) {
	p.parseFieldContent(con, fmt.Sprintf("avocet.parsers.rst.section.fields.%s", p.prime.Field))
}

func (p *Parser) parseDocumentField(con *ast.Content) {
	p.parseFieldContent(con, fmt.Sprintf("avocet.parsers.rst.document.fields.%s", p.prime.Field))
}

func (p *Parser) parseFieldContent(con *ast.Content, confKey string) {
	target := viper.GetString(confKey)
	key := p.prime.Field

	switch target {
	case "bool", "boolean":
		if p.prime.Text == "false" {
			con.Meta[key] = false
		} else if p.prime.Text == "true" || p.prime.Text == "" {
			con.Meta[key] = true
		}
	case "str", "string":
		con.Meta[key] = p.prime.Text
	case "int", "integer":
		if val, err := strconv.Atoi(p.prime.Text); err == nil {
			con.Meta[key] = val
		}
	case "content":
		f := p.prime.File
		toks := []*token.Token{p.prime}
		for p.secunde.Type == token.EMPTY || (p.secunde.Type == token.INDENT && p.secunde.Indent > 2) {
			toks = append(toks, p.secunde)
			p.read()
		}
		sp := newToks(toks, f)
		scon := ast.NewContent()
		sp.Parse(scon)
		p.Merge(sp)
		con.Meta[key] = scon
	default:
		p.lgr.SetPrefix(fmt.Sprintf("%s.%d", p.prefix, p.prime.Line))
		p.lgr.Warnf("Unknown metadata key: %s\n", key)
		p.lgr.SetPrefix(p.prefix)
	}
}

func (p *Parser) parseComment(con *ast.Content) {
	text := []string{p.prime.Text}

	blk := ast.NewBlock(p.prime.File, p.prime.Line)
	blk.Set(ast.COMMENT)

	for p.secunde.Type == token.EMPTY || (p.secunde.Type == token.INDENT && p.secunde.Indent > 2) {
		p.secunde.Dedent(3)
		text = append(text, p.secunde.Text)
		p.read()
	}

	blk.Content.Meta["__COMMENT__"] = strings.Join(text, "\n")
	p.AddBlock(blk, con)
}

func (p *Parser) parseFootnote(con *ast.Content) {
	key := p.prime.Field
	toks := []*token.Token{p.prime}
	f := p.prime.File

	for p.secunde.Type == token.EMPTY || (p.secunde.Type == token.INDENT && p.secunde.Indent > 2) {
		p.secunde.Dedent(3)
		toks = append(toks, p.secunde)
		p.read()
	}
	sp := newToks(toks, f)
	c := ast.NewContent()
	sp.Parse(c)
	p.Merge(sp)

	con.Notes[key] = c

}

func (p *Parser) parseTarget(con *ast.Content) {
	key := p.prime.Field

	if p.prime.Text == "" {
		p.idrefs = append(p.idrefs, key)
		return
	}
	e := ast.NewElement(p.prime.File, p.prime.Line, 1)
	e.Set(ast.TEXT)
	e.Text = p.prime.Text
	p.targets[key] = e
}
