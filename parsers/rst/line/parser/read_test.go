package parser

import (
	"fmt"
	"strings"
	"testing"

	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet/ast"
)

func Test_block_parser(t *testing.T) {
	data := []struct {
		r string
		t ast.BType
		s int
		k string
	}{
		{"Text", ast.PARA, 1, ""},
		{"* Item\n* Item", ast.ILIST, 1, ""},
		{"- Item\n- Item", ast.ILIST, 1, ""},
		{"+ Item\n+ Item", ast.ILIST, 1, ""},
		{"+ Item\n* Item", ast.ILIST, 1, ""},
		{"* Item\n- Item", ast.ILIST, 1, ""},
		{"* [ ] Item\n* Item", ast.ILIST, 1, " "},
		{"- [o] Item\n- Item", ast.ILIST, 1, "o"},
		{"+ [x] Item\n+ Item", ast.ILIST, 1, "x"},
		{"+ [i] Item\n* Item", ast.ILIST, 1, "i"},
		{"* [-] Item\n- Item", ast.ILIST, 1, "-"},
		{"#. Item", ast.ELIST, 1, ""},
		{"A. Item", ast.ELIST, 1, ""},
		{"1. Item", ast.ELIST, 1, ""},
		{"i. Item", ast.ELIST, 1, ""},
		{".. comment", ast.COMMENT, 1, ""},
	}

	for _, datum := range data {
		p := New(datum.r, "read_test.go")
		con := ast.NewContent()
		p.Parse(con)

		size := len(con.Data)
		if size != datum.s {
			t.Errorf("parser returned wrong blocks: %d != %d", size, datum.s)
			continue
		}

		blk := con.Data[0]

		if blk.Type != datum.t {
			t.Errorf("parser returned wrong data: %s != %s", blk.Type, datum.t)
			continue
		}
		if blk.Type == ast.ILIST && len(blk.Content.Data) > 0 && datum.k != "" {
			nblk := blk.Content.Data[0]
			fmt.Printf("%q == %q\n", nblk.Key, datum.k)

			if nblk.Key != datum.k {
				t.Errorf("parser set wrong key on %s: %q != %q", nblk.Type, nblk.Key, datum.k)
				continue
			}
		}
	}
}

func Test_definition_list(t *testing.T) {
	data := []struct {
		r string
		t string
		d string
		c []string
	}{

		{"term\n   Text", "term", "Text", []string{}},
		{"term : item\n   Text", "term", "Text", []string{"item"}},
		{"term : item : other\n   Text", "term", "Text", []string{"item", "other"}},
	}

	for _, datum := range data {
		p := New(datum.r, "read_test.go")
		con := ast.NewContent()
		p.Parse(con)
		size := len(con.Data)
		if size != 1 {
			t.Errorf("parser failed to find definition list: %d != 1", size)
			continue
		}
		dlist := con.Data[0]

		if dlist.Type != ast.DLIST {
			t.Errorf("parser set wrong type: %s != %s", dlist.Type, ast.DLIST)
			continue
		}
		size = len(dlist.Content.Data)
		if size != 1 {
			t.Errorf("parser failed to set definition list item: %d != 1", size)
			continue
		}
		dli := dlist.Content.Data[0]

		if dli.Type != ast.DLISTITEM {
			t.Errorf("parser set wrong definition list item type: %s != %s", dli.Type, ast.DLISTITEM)
			continue
		}
		size = len(dli.Content.Data)

		if size != 2 {
			t.Errorf("parser set wrong number of entries in definition list item: %d != 2", size)
			continue
		}
		dt := dli.Content.Data[0]
		dd := dli.Content.Data[1]

		if dt.Type != ast.TERM {
			t.Errorf("parser set wrong term type: %s != %s", dt.Type, ast.TERM)
			continue
		}

		if dd.Type != ast.DEF {
			t.Errorf("parser set wrong definition type: %s != %s", dd.Type, ast.DEF)
			continue
		}

		if len(datum.c) > 0 {
			clses := dli.Content.GetStringArray("__CLASSIFIERS__")
			size = len(clses)
			csize := len(datum.c)

			if size != csize {
				t.Errorf("parser set wrong classifiers: %d != %d", size, csize)
				continue
			}

			for i, cls := range datum.c {
				ccls := clses[i]

				if ccls != cls {
					t.Errorf("parser set wrong classifier at %d: %q != %q", i, ccls, cls)
					continue
				}
			}

		}

	}

}

func Test_fieldlist(t *testing.T) {
	data := []string{
		":boolean:",
		":string: value",
		":integer: 1",
	}
	m := map[string]any{
		"boolean": true,
		"string":  "value",
		"integer": 1,
	}

	viper.SetDefault("avocet.parsers.rst.document.fields.boolean", "boolean")
	viper.SetDefault("avocet.parsers.rst.document.fields.string", "string")
	viper.SetDefault("avocet.parsers.rst.document.fields.integer", "int")

	p := New(strings.Join(data, "\n"), "read_test.go")
	con := ast.NewContent()
	p.Parse(con)

	for k, v := range m {
		val, ok := con.Meta[k]
		if !ok {
			t.Errorf("Meta: %s", con.Meta)
			t.Errorf("parser failed to set value for %q", k)
			continue
		}
		if v != val {
			t.Errorf("parser set wrong value for %q: %q != %q", k, v, val)
			continue
		}
	}

}

func Test_footnotes(t *testing.T) {
	data := []struct {
		r string
		k string
	}{
		{".. [1] Text", "1"},
	}

	for _, datum := range data {
		p := New(datum.r, "read_test.go")
		con := ast.NewContent()
		p.Parse(con)

		_, ok := con.Notes[datum.k]
		if !ok {
			t.Errorf("parser failed to set footnote for %q", datum.k)
		}
	}
}

func Test_targets(t *testing.T) {
	raw := []string{
		".. _key: target",
		".. _`tick key`: target",
		"",
		".. _para-key:",
		".. _`tick para key`:",
		"",
		"Text",
	}
	p := New(strings.Join(raw, "\n"), "read_test.go")
	con := ast.NewContent()
	_, targets := p.Parse(con)

	data := map[string]string{
		"key":      "target",
		"tick key": "target",
	}
	keys := []string{"para-key", "tick para key"}

	for k, v := range data {
		val, ok := targets[k]
		if !ok {
			t.Errorf("targets missing key: %s\n%s", k, targets)
			continue
		}
		if val.Text != v {
			t.Errorf("targets set wrong value on %s: %q != %q", k, val, v)
			continue
		}
	}
	size := len(con.Data)
	if size != 1 {
		t.Errorf("parser return wrong blocks: %d != 1", size)
		return
	}
	blk := con.Data[0]
	size = len(blk.Idrefs)
	csize := len(keys)
	if size != csize {
		t.Errorf("parser set wrong identity reference: %d != %d", size, csize)
		return
	}
	for i, k := range keys {
		ck := blk.Idrefs[i]
		if k != ck {
			t.Errorf("parser set wrong key: %s != %s", k, ck)
			continue
		}
	}
}
