package token

// DeIList updates the Token.Runes, .Type, .Text, and .Size to
// accommodate the removal of IList characters. It is called by
// the Lexer to clean up the text for subparsing.
func (tok *Token) DeIList() {
	tok.Set(ILIST)
	tok.Runes = tok.Runes[1:]
	var i int
	var r rune
	for i, r = range tok.Runes {
		if r != ' ' {
			tok.Runes = tok.Runes[i:]
			break
		}
	}
	tok.Size = len(tok.Runes)
	tok.Text = string(tok.Runes)
}

// DeCList updates the token.Runes, .Type, .Text, and .Size to
// accommodate the removal of CList characters. It is called by
// the Lexer to clean up the text for subparsing.
func (tok *Token) DeCList() {
	tok.Set(CLIST)
	tok.Key = tok.Get(3)
	if tok.Size > 5 {
		var i int
		var r rune
		for i, r = range tok.Runes[5:] {
			if r != ' ' {
				tok.Runes = tok.Runes[i+5:]
				break
			}
		}
		tok.Size = len(tok.Runes)
		tok.Text = string(tok.Runes)
		return
	}
	tok.Runes = []rune{}
	tok.Size = 0
	tok.Text = ""
}

// DeHashList removes hash or single character and period to
// prep an enumerated list item token for further parsing.
func (tok *Token) DeHashList() {
	tok.Set(ELIST)
	if tok.Size > 3 {
		var i int
		var r rune
		for i, r = range tok.Runes[3:] {
			if r != ' ' {
				tok.Runes = tok.Runes[i+3:]
				break
			}
		}
		tok.Size = len(tok.Runes)
		tok.Text = string(tok.Runes)
		return
	}
	tok.Runes = []rune{}
	tok.Size = 0
	tok.Text = ""

}

// DeDot remotes leading dots from the token
func (tok *Token) DeDot() {
	if tok.Size <= 3 {
		tok.Text = ""
		tok.Runes = []rune{}
		tok.Size = 0
		return
	}
	tok.Runes = tok.Runes[3:]
	tok.Text = string(tok.Runes)
	tok.Size = len(tok.Runes)

}
