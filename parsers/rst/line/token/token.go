package token

// Token provides a struct for the linear Lexer to store data on
// during lexical analysis.
type Token struct {
	Type Type   `json:"type" yaml:"type"`
	Name string `json:"type_name" yaml:"type_name"`

	File string `json:"file" yaml:"file"`
	Line int    `json:"line" yaml:"line"`

	Text   string `json:"text" yaml:"text"`
	Runes  []rune `json:"-" yaml:"-"`
	Size   int    `json:"size" yaml:"size"`
	Indent int    `json:"indent,omitempty" yaml:"indent,omitempty"`
	Key    rune   `json:"-" yaml:"-"`
	Field  string `json:"field,omitempty" yaml:"field,omitempty"`
}

// New takes a text string, a filename, and a line number and
// returns a *Token for the Lexer to analyze.
func New(text, file string, line int) *Token {
	tok := &Token{
		File: file,
		Line: line,
	}
	tok.Retext(text)
	return tok
}

// Retext takes a text string and updates the Text, Runes, and
// Size attributes.
func (tok *Token) Retext(text string) {
	tok.Text = text
	tok.Runes = []rune(text)
	tok.Size = len(tok.Runes)
}

// Set applies the given Type and its string representation to
// the Token.
func (tok *Token) Set(t Type) {
	tok.Type = t
	tok.Name = t.String()
}

// Get returns a rune for the given index.
func (tok *Token) Get(index int) rune {
	if index < tok.Size {
		return tok.Runes[index]
	}
	return 0
}

// CountWS counts the number of spaces at the start of the line
// and sets it on the Indent attribute.
func (tok *Token) CountWS() {
	count := 0
	for _, r := range tok.Runes {
		if r == ' ' || r == '\t' {
			count++
			continue
		}
		break
	}
	tok.Indent = count
}

// Dedent removes space indentation to prepare the Token for
// subparsing.
func (tok *Token) Dedent(space int) {
	if len(tok.Runes) > space {
		tok.Runes = tok.Runes[space:]
		tok.Text = string(tok.Runes)
		tok.Size = len(tok.Runes)
		return
	}

	tok.Runes = []rune{}
	tok.Text = ""
	tok.Size = 0
	tok.Type = EMPTY

}
