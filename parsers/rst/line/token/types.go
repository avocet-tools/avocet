package token

import "fmt"

// Type specifies the type of line encountered by the Lexer.
type Type int

const (
	// UNSET indicates a line that has not yet been analyzed by
	// the Lexer.
	UNSET Type = iota

	// EOF indicates that the Lexer has reached the end of the
	// file.
	EOF

	// EMPTY indicates a line that is empty or which only
	// contains whitespace characters.
	EMPTY

	// TEXT indicates a text line.
	TEXT

	// HEAD indicates a row of headline characters.
	HEAD

	// INDENT indicates an indented line.
	INDENT

	// ILIST indicates an itemized list item.
	ILIST

	// CLIST indicates an itemized checklist item.
	CLIST

	// ELIST indicates an enumerated list item
	ELIST

	// META indicates a field list item
	META

	// FNOTE indicates a footnote line.
	FNOTE

	// COMMENT indicates a comment line.
	COMMENT

	// TARGET indicates a target line.
	TARGET
)

const (
	unsetType   = "UNSET_TOKEN"
	eofType     = "EOF_TOKEN"
	emptyType   = "EMPTY_TOKEN"
	textType    = "TEXT_TOKEN"
	headType    = "HEADLINE_TOKEN"
	indentType  = "INDENT_TOKEN"
	ilistType   = "ITEMIZED_LIST_ITEM"
	clistType   = "ITEMIZED_CHECK_LIST_ITEM"
	elistType   = "ENUMERATED_LIST_ITEM"
	metaType    = "FIELD_LIST_ITEM"
	fnoteType   = "FOOTNOTE_TOKEN"
	commentType = "COMMENT_TOKEN"
	targetType  = "TARGET_TOKEN"
)

var (
	typeString = map[Type]string{
		UNSET:   unsetType,
		EOF:     eofType,
		EMPTY:   emptyType,
		TEXT:    textType,
		HEAD:    headType,
		INDENT:  indentType,
		ILIST:   ilistType,
		CLIST:   clistType,
		ELIST:   elistType,
		META:    metaType,
		FNOTE:   fnoteType,
		COMMENT: commentType,
		TARGET:  targetType,
	}

	stringType = map[string]Type{
		unsetType:   UNSET,
		eofType:     EOF,
		emptyType:   EMPTY,
		textType:    TEXT,
		headType:    HEAD,
		indentType:  INDENT,
		ilistType:   ILIST,
		clistType:   CLIST,
		elistType:   ELIST,
		metaType:    META,
		fnoteType:   FNOTE,
		commentType: COMMENT,
		targetType:  TARGET,
	}
)

// String returns the string representation of the Type.
func (t Type) String() string {
	ret, ok := typeString[t]
	if ok {
		return ret
	}
	return fmt.Sprintf("UNKNOWN_TOKEN_%d", t)
}

// StringToType returns the Type for the given string.
func StringToType(name string) Type {
	ret, ok := stringType[name]
	if ok {
		return ret
	}
	return UNSET
}
