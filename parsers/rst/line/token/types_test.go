package token

import "testing"

func Test_token_name(t *testing.T) {
	data := []struct {
		t Type
		s string
	}{
		{UNSET, unsetType},
		{EOF, eofType},
		{EMPTY, emptyType},
		{TEXT, textType},
		{HEAD, headType},
		{INDENT, indentType},
		{ILIST, ilistType},
		{CLIST, clistType},
		{ELIST, elistType},
		{META, metaType},
		{FNOTE, fnoteType},
		{COMMENT, commentType},
		{TARGET, targetType},
	}

	for _, datum := range data {
		if datum.t.String() != datum.s {
			t.Errorf("invalid type string: %s != %s", datum.t, datum.s)
			continue
		}

		st := StringToType(datum.s)
		if st != datum.t {
			t.Errorf("invalid string type: %s != %s", st, datum.t)
			continue
		}

	}

}
