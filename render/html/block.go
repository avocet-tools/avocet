package html

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderBlocks takes a slice of ast.Blocks and returns an HTML
// formatted string.
func (r Renderer) RenderBlocks(blks []*ast.Block) string {
	text := []string{}
	for _, blk := range blks {
		text = append(text, r.RenderBlock(blk))
	}
	return strings.Join(text, "")
}

// RenderBlock takes an ast.Block and returns an HTML formatted
// string.
func (r Renderer) RenderBlock(blk *ast.Block) string {
	switch blk.Type {
	case ast.ILIST:
		return fmt.Sprintf("<ul>%s</ul>", r.RenderBlocks(blk.Content.Data))
	case ast.ELIST:
		return fmt.Sprintf("<ol>%s</ol>", r.RenderBlocks(blk.Content.Data))
	case ast.ILISTITEM, ast.ELISTITEM:
		return fmt.Sprintf("<li>%s</li>", r.RenderBlocks(blk.Content.Data))
	case ast.PARA:
		return fmt.Sprintf("<p>%s</p>", r.RenderElements(blk.Content.Line))
	case ast.DLIST:
		return fmt.Sprintf("<dl>%s</dl>", r.RenderBlocks(blk.Content.Data))
	case ast.DLISTITEM:
		return r.RenderBlocks(blk.Content.Data)
	case ast.TERM:
		return fmt.Sprintf("<dt>%s</dt>", r.RenderElements(blk.Content.Line))
	case ast.DEF:
		return fmt.Sprintf("<dd>%s</dd>", r.RenderBlocks(blk.Content.Data))
	default:
		return ""
	}
}
