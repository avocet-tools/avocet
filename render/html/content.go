package html

import (
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderContent takes an ast.Content and returns a HTML
// formatted string.
func (r Renderer) RenderContent(con *ast.Content) string {
	text := []string{}

	text = append(text, r.RenderBlocks(con.Data))

	text = append(text, r.RenderSections(con.Sects))

	return strings.Join(text, "")
}
