package html

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderElements takes a slice of ast.Elements and returns an
// HTML formatted string.
func (r Renderer) RenderElements(es []*ast.Element) string {
	text := []string{}
	for _, e := range es {
		text = append(text, r.RenderElement(e))
	}
	return strings.Join(text, "")
}

// RenderElement takes an ast.Element and returns an HTML
// formatted string.
func (r Renderer) RenderElement(e *ast.Element) string {
	switch e.Type {
	case ast.STRONG:
		return fmt.Sprintf("<strong>%s</strong>", e.Text)
	case ast.EMPHASIS:
		return fmt.Sprintf("<em>%s</em>", e.Text)
	case ast.LITERAL:
		return fmt.Sprintf("<code>%s</code>", e.Text)
	case ast.FOOT:
		return fmt.Sprintf("<a class=\"footnote\" href=\"#%s\">%s</span>", e.Key, e.Text)
	default:
		return e.Text
	}
}
