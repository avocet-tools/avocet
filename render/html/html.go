package html

import (
	"os"

	"github.com/charmbracelet/log"
)

// Renderer provides methods to render Avocet AST to HTML
// formatted strings.
type Renderer struct {
	lgr    *log.Logger
	prefix string
}

// New initializes and returns an Renderer.
func New() Renderer {
	prefix := "htmlRenderer"
	return Renderer{
		prefix: prefix,
		lgr: log.NewWithOptions(
			os.Stderr,
			log.Options{
				ReportTimestamp: false,
				Level:           log.DebugLevel,
				Prefix:          prefix,
			},
		),
	}
}
