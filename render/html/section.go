package html

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderSections takes a slice of ast.Sections and returns a
// HTML formatted string.
func (r Renderer) RenderSections(sects []*ast.Section) string {
	text := []string{}

	for _, sect := range sects {
		text = append(text, r.RenderSection(sect))
	}

	return strings.Join(text, "")
}

// RenderSection takes an ast.Section and returns an HTML
// formatted string.
func (r Renderer) RenderSection(sect *ast.Section) string {
	text := []string{"<div class=\"section\">"}
	if len(sect.Content.Line) > 0 {
		text = append(text, fmt.Sprintf(
			"<h%d>%s</h%d>", sect.Level.Global, r.RenderElements(sect.Content.Line), sect.Level.Global))
	}

	text = append(text, r.RenderContent(sect.Content))

	text = append(text, "</div>")

	return strings.Join(text, "")
}
