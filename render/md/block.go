package md

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderBlocks takes a slice of ast.Blocks and returns a
// Markdown formatted string.
func (r Renderer) RenderBlocks(blks []*ast.Block) string {
	text := []string{}

	for i, blk := range blks {
		if st := r.RenderBlock(blk, i); st != "" {
			text = append(text, st)
		}
	}

	return strings.Join(text, "\n\n")
}

// RenderBlock takes an ast.Block and returns a Markdown
// formatted string.
func (r Renderer) RenderBlock(blk *ast.Block, index int) string {
	switch blk.Type {
	case ast.ILISTITEM:
		rs := []rune(r.ilistStyle.Render(r.RenderBlocks(blk.Content.Data)))
		if len(rs) == 0 {
			return ""
		}
		return fmt.Sprintf("-%s", string(rs[1:]))
	case ast.ILIST:
		return r.RenderBlocks(blk.Content.Data)

	case ast.PARA:
		return r.RenderElements(blk.Content.Line)
	case ast.ELISTITEM:
		num := "1. "
		size := len([]rune(num))
		rs := []rune(r.elistStyle.Render(r.RenderBlocks(blk.Content.Data)))
		if len(rs) < size {
			return num
		}
		return fmt.Sprintf("%s%s", num, string(rs[size:]))
	case ast.ELIST:
		return r.RenderBlocks(blk.Content.Data)
	case ast.DLIST:
		return r.RenderBlocks(blk.Content.Data)
	case ast.DLISTITEM:
		term := r.termStyle.Render(r.RenderElements(blk.Content.Data[0].Content.Line))
		def := r.defStyle.Render(r.RenderBlocks(blk.Content.Data[1].Content.Data))
		return fmt.Sprintf("%s\n%s", term, def)
	}
	return ""
}
