package md

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderElements takes a slice of ast.Elements and returns a
// Markdown formatted string.
func (r Renderer) RenderElements(es []*ast.Element) string {
	text := []string{}

	for _, e := range es {
		text = append(text, r.RenderElement(e))
	}
	return strings.Join(text, "")
}

// RenderElement takes an ast.Element and returns a Markdown
// formatted string.
func (r Renderer) RenderElement(e *ast.Element) string {
	switch e.Type {
	case ast.STRONG:
		return fmt.Sprintf("**%s**", e.Text)
	case ast.EMPHASIS:
		return fmt.Sprintf("*%s*", e.Text)
	case ast.LITERAL:
		return fmt.Sprintf("`%s`", e.Text)
	case ast.FOOT:
		return ""
	default:
		return e.Text
	}
}
