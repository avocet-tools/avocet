package md

import (
	"os"

	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/log"
	"github.com/spf13/viper"
)

// Renderer provides methods to render Avocet AST to Markdown.
type Renderer struct {
	prefix     string
	lgr        *log.Logger
	ilistStyle lipgloss.Style
	elistStyle lipgloss.Style
	termStyle  lipgloss.Style
	defStyle   lipgloss.Style
}

// New returns a Renderer to render Avocet AST to Markdown.
func New() Renderer {
	prefix := "markdownRenderer"
	return Renderer{
		prefix: prefix,
		lgr: log.NewWithOptions(
			os.Stderr,
			log.Options{
				ReportTimestamp: false,
				Level:           log.DebugLevel,
				Prefix:          prefix,
			},
		),
		ilistStyle: lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width")).PaddingLeft(2),
		elistStyle: lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width")).PaddingLeft(3),
		termStyle:  lipgloss.NewStyle().Bold(true),
		defStyle:   lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width")).PaddingLeft(3).PaddingTop(0).MarginTop(0),
	}

}
