package md

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderSections takes a slice of ast.Sections and returns a
// Markdown formatted string.
func (r Renderer) RenderSections(sects []*ast.Section) string {
	text := []string{}

	for _, sect := range sects {
		text = append(text, r.RenderSection(sect))
	}

	return strings.Join(text, "\n\n")
}

// RenderSection takes an ast.Section and returns a Markdown
// formatted string.
func (r Renderer) RenderSection(sect *ast.Section) string {
	headChar := strings.Repeat("#", sect.Level.Global)
	title := r.RenderElements(sect.Content.Line)

	text := []string{fmt.Sprintf("%s %s", headChar, title)}
	text = append(text, r.renderContent(sect.Content))

	return strings.Join(text, "\n\n")
}
