package render

import (
	"gitlab.com/avocet-tools/avocet/render/html"
	"gitlab.com/avocet-tools/avocet/render/md"
	"gitlab.com/avocet-tools/avocet/render/rst"
	"gitlab.com/avocet-tools/avocet/render/stdout"
	"gitlab.com/avocet-tools/avocet/render/tex"
)

// Stdout returns a stdout configured Renderer.
func Stdout() stdout.Renderer {
	return stdout.New()
}

// HTML returns an HTML configured Renderer.
func HTML() html.Renderer {
	return html.New()
}

// RST returns an RST configured Renderer.
func RST() rst.Renderer {
	return rst.New()
}

// TeX returns a TeX configured Renderer
func TeX() tex.Renderer {
	return tex.New()
}

// MD returns a Markdown configured Renderer
func MD() md.Renderer {
	return md.New()
}
