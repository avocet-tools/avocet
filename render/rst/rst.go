package rst

import (
	"fmt"
	"os"

	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/log"
	"github.com/spf13/viper"
	"golang.org/x/term"
)

// Renderer provides methods to take Avocet AST structs and
// return reStructuredText formatted strings.
type Renderer struct {
	lgr        *log.Logger
	prefix     string
	ilistStyle lipgloss.Style
	elistStyle lipgloss.Style
	termStyle  lipgloss.Style
	defStyle   lipgloss.Style
}

// New initializes and returns a Renderer to render AST to
// reStructuredText.
func New() Renderer {
	prefix := "rstRenderer"

	r := Renderer{
		prefix: prefix,
		lgr: log.NewWithOptions(
			os.Stderr,
			log.Options{
				ReportTimestamp: false,
				Level:           log.DebugLevel,
				Prefix:          prefix,
			},
		),

		ilistStyle: lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width")).PaddingLeft(2),
		elistStyle: lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width")).PaddingLeft(3),
		termStyle:  lipgloss.NewStyle().Bold(true),
		defStyle:   lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width")).PaddingLeft(3).PaddingTop(0).MarginTop(0),
	}
	return r
}

func init() {
	headChars := []string{
		"#", "*", "=", "-", "~", "^", "!", "\"", "$", "%", "&", "'", "(", ")",
		"+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "@", "[",
		"]", "^", "_", "`", "{", "|", "}"}
	for lvl, char := range headChars {
		viper.SetDefault(fmt.Sprintf("avocet.render.rst.headline.%d", lvl+1), char)
	}
	w, _, err := term.GetSize(1)
	if err == nil && w > 83 {
		w = 83
	}
	viper.SetDefault("avocet.render.rst.width", w)

}
