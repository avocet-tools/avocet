package rst

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderSections takes a slice of ast.Sections and returns a
// reStructuredText formatted string.
func (r Renderer) RenderSections(sects []*ast.Section) string {
	text := []string{}

	for _, sect := range sects {
		text = append(text, r.RenderSection(sect))
	}

	return strings.Join(text, "\n\n")
}

// RenderSection takes an ast.Section and returns a
// reStructuredText formatted string.
func (r Renderer) RenderSection(sect *ast.Section) string {
	text := []string{}
	headChar := viper.GetString(fmt.Sprintf("avocet.render.rst.headline.%d", sect.Level.Global))
	title := r.RenderElements(sect.Content.Line)

	headline := strings.Repeat(headChar, len(title))

	if sect.Level.Global == 1 {
		text = append(text, fmt.Sprintf("%s\n%s\n%s", headline, title, headline))
	} else {
		text = append(text, fmt.Sprintf("%s\n%s", title, headline))
	}

	text = append(text, r.renderContent(sect.Content))

	return strings.Join(text, "\n\n")
}
