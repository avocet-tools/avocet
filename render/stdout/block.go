package stdout

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderBlocks takes a slice of ast.Blocks and returns a stdout
// formatted string.
func (r Renderer) RenderBlocks(blks []*ast.Block) string {
	text := []string{}
	for i, blk := range blks {
		text = append(text, r.RenderBlock(blk, i))
	}
	return strings.Join(text, "\n\n")
}

// RenderBlock takes an ast.Block and returns a stdout formatted
// string.
func (r Renderer) RenderBlock(blk *ast.Block, index int) string {
	switch blk.Type {
	case ast.PARA:
		return r.RenderElements(blk.Content.Line)
	case ast.ILISTITEM:
		rs := []rune(r.ilistStyle.Render(r.RenderBlocks(blk.Content.Data)))
		if len(rs) == 0 {
			return ""
		}
		return fmt.Sprintf("-%s", string(rs[1:]))
	case ast.ILIST:
		return r.RenderBlocks(blk.Content.Data)
	case ast.ELIST:
		return r.RenderBlocks(blk.Content.Data)
	case ast.ELISTITEM:
		num := fmt.Sprintf("%d. ", index+1)
		size := len([]rune(num))
		rs := []rune(r.elistStyle.Render(r.RenderBlocks(blk.Content.Data)))
		if len(rs) < size {
			return num
		}
		return fmt.Sprintf("%s%s", num, string(rs[size:]))
	case ast.DLIST:
		return r.RenderBlocks(blk.Content.Data)
	case ast.DLISTITEM:
		term := blk.Content.Data[0]
		def := blk.Content.Data[1]
		ret := []rune(r.defStyle.Render(fmt.Sprintf("%s, %s",
			r.termStyle.Render(r.RenderElements(term.Content.Line)),
			r.RenderBlocks(def.Content.Data))))
		ret[0] = '-'
		return string(ret)
	default:
		r.lgr.Warnf("Unsupported block for string rendering: %s", blk.Type)
		return ""
	}
}
