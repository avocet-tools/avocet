package stdout

import (
	"strings"

	"github.com/charmbracelet/lipgloss"
	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderContent takes an ast.Content and returns a stdout
// formatted string.
func (r Renderer) RenderContent(con *ast.Content) string {
	contentStyle := lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width"))
	return contentStyle.Render(r.renderContent(con))
}

func (r Renderer) renderContent(con *ast.Content) string {
	text := []string{}
	if s := r.RenderBlocks(con.Data); s != "" {
		text = append(text, s)
	}
	if s := r.RenderSections(con.Sects); s != "" {
		text = append(text, s)
	}

	return strings.Join(text, "\n\n")
}
