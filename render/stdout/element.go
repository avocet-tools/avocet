package stdout

import (
	"fmt"
	"strings"

	"github.com/charmbracelet/lipgloss"
	"gitlab.com/avocet-tools/avocet/ast"
)

var (
	emphatic = lipgloss.NewStyle().Italic(true)
	strong   = lipgloss.NewStyle().Bold(true)
	literal  = lipgloss.NewStyle().Bold(true).Italic(true)
	footnote = lipgloss.NewStyle().Bold(true).Italic(true)
)

// RenderElements takes a slice of ast.Elements and returns a
// stdout formatted string.
func (r Renderer) RenderElements(es []*ast.Element) string {
	text := []string{}
	for _, e := range es {
		text = append(text, r.RenderElement(e))
	}

	return strings.Join(text, "")
}

// RenderElement takes an ast.Element and returns a stdout
// formatted string.
func (r Renderer) RenderElement(e *ast.Element) string {
	switch e.Type {
	case ast.TEXT:
		return e.Text
	case ast.LITERAL:
		return literal.Render(e.Text)
	case ast.STRONG:
		return strong.Render(e.Text)
	case ast.EMPHASIS:
		return emphatic.Render(e.Text)
	case ast.FOOT:
		return footnote.Render(fmt.Sprintf("^%s", e.Key))
	}
	r.lgr.Errorf("Unable to render %s to stdout, returning as unadorned string", e.Type)
	return e.Text
}
