package stdout

import (
	"fmt"
	"strings"

	"github.com/charmbracelet/lipgloss"
	"gitlab.com/avocet-tools/avocet/ast"
)

var (
	titleFormat = lipgloss.NewStyle().Bold(true)
	charFormat  = lipgloss.NewStyle().Bold(true)
)

// RenderSections takes a slice of ast.Sections and returns a
// stdout formatted string.
func (r Renderer) RenderSections(sects []*ast.Section) string {
	text := []string{}

	for _, sect := range sects {
		text = append(text, r.RenderSection(sect))
	}

	return strings.Join(text, "\n\n")
}

// RenderSection takes an ast.Section and returns a stdout
// formatted string.
func (r Renderer) RenderSection(sect *ast.Section) string {
	titleText := fmt.Sprintf("%s %s",
		charFormat.Render(strings.Repeat("#", sect.Level.Global)),
		titleFormat.Render(r.RenderElements(sect.Content.Line)))
	text := []string{titleText}

	text = append(text, r.RenderContent(sect.Content))

	return strings.Join(text, "\n\n")
}
