package stdout

import (
	"os"

	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/log"
	"github.com/spf13/viper"
)

// Renderer provides functions to render the Avocet AST to
// stdout formatted strings.
type Renderer struct {
	lgr        *log.Logger
	prefix     string
	ilistStyle lipgloss.Style
	elistStyle lipgloss.Style
	termStyle  lipgloss.Style
	defStyle   lipgloss.Style
}

// New initializes and returns a new Renderer.
func New() Renderer {
	prefix := "stdoutRender"
	return Renderer{
		lgr: log.NewWithOptions(
			os.Stderr,
			log.Options{
				ReportTimestamp: false,
				Level:           log.DebugLevel,
				Prefix:          prefix,
			},
		),
		ilistStyle: lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width")).PaddingLeft(2),
		elistStyle: lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width")).PaddingLeft(3),
		termStyle:  lipgloss.NewStyle().Bold(true),
		defStyle:   lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width")).PaddingLeft(3),
		prefix:     prefix,
	}
}
