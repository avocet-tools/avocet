package tex

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderBlocks takes a slice of ast.Blocks and returns a TeX
// formatted string.
func (r Renderer) RenderBlocks(blks []*ast.Block) string {
	text := []string{}
	for _, blk := range blks {
		if s := r.RenderBlock(blk); s != "" {
			text = append(text, s)
		}
	}
	return strings.Join(text, "\n\n")
}

// RenderBlock takes an ast.Block and returns a TeX formatted
// string.
func (r Renderer) RenderBlock(blk *ast.Block) string {
	switch blk.Type {
	case ast.PARA:
		return r.RenderElements(blk.Content.Line)
	case ast.ILIST:
		return fmt.Sprintf("\\begin{itemize}\n%s\\end{itemize}\n", r.RenderBlocks(blk.Content.Data))
	case ast.ILISTITEM, ast.ELISTITEM:
		return fmt.Sprintf("\\item %s\n", r.RenderBlocks(blk.Content.Data))
	case ast.ELIST:
		return fmt.Sprintf("\\begin{enumerate}\n%s\\end{enumerate}\n", r.RenderBlocks(blk.Content.Data))
	}
	return ""
}
