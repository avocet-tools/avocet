package tex

import (
	"strings"

	"github.com/charmbracelet/lipgloss"
	"github.com/spf13/viper"
	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderContent takes an ast.Content and returns a TeX
// formatted string.
func (r Renderer) RenderContent(con *ast.Content) string {
	contentStyle := lipgloss.NewStyle().Width(viper.GetInt("avocet.render.rst.width"))

	return contentStyle.Render(r.renderContent(con))
}

func (r Renderer) renderContent(con *ast.Content) string {
	text := []string{}

	text = append(text, r.RenderBlocks(con.Data))

	text = append(text, r.RenderSections(con.Sects))

	return strings.Join(text, "\n\n")
}
