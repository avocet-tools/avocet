package tex

import (
	"fmt"
	"strings"

	"gitlab.com/avocet-tools/avocet/ast"
)

// RenderSections takes a slice of ast.Section and returns a TeX
// formatted string.
func (r Renderer) RenderSections(sects []*ast.Section) string {
	text := []string{}
	for _, sect := range sects {
		text = append(text, r.RenderSection(sect))
	}

	return strings.Join(text, "\n\n")
}

// RenderSection takes an ast.Section and returns a TeX
// formatted string.
func (r Renderer) RenderSection(sect *ast.Section) string {
	sectName, ok := sectNames[sect.Level.Global]
	if !ok {
		sectName = defSect
	}
	title := r.RenderElements(sect.Content.Line)

	text := []string{fmt.Sprintf("\\%s{%s}", sectName, title)}

	text = append(text, r.renderContent(sect.Content))

	return strings.Join(text, "\n\n")
}

var (
	sectNames = map[int]string{
		0: "setTitle",
		1: "part",
		2: "chapter",
		3: "section",
		4: "subsection",
		5: "subsubsection",
		6: "paragraph",
		7: "subparagraph",
	}
	defSect = sectNames[len(sectNames)-1]
)
