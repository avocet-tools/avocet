package tex

import (
	"os"

	"github.com/charmbracelet/log"
)

// Renderer provides methods to render Avocet AST to TeX
// formatted strings.
type Renderer struct {
	lgr    *log.Logger
	prefix string
}

// New returns a Renderer ready to format Avocet AST to TeX.
func New() Renderer {
	prefix := "texRenderer"
	return Renderer{
		prefix: prefix,
		lgr: log.NewWithOptions(
			os.Stderr,
			log.Options{
				ReportTimestamp: false,
				Level:           log.DebugLevel,
				Prefix:          prefix,
			},
		),
	}
}
